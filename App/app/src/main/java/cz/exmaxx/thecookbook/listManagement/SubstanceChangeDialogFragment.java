package cz.exmaxx.thecookbook.listManagement;

import android.app.Activity;
import android.content.DialogInterface;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import cz.exmaxx.thecookbook.R;
import cz.exmaxx.thecookbook.db.Substance;

/**
 * Created by maxx on 3.9.2014.
 */
public class SubstanceChangeDialogFragment extends android.support.v4.app.DialogFragment
{
    Substance                       mSubstance;
    SubstanceListManagementFragment mManager;

    public static SubstanceChangeDialogFragment newInstance( SubstanceListManagementFragment manager ) {
        SubstanceChangeDialogFragment fragment = new SubstanceChangeDialogFragment();
        fragment.mManager = manager;
        fragment.mSubstance = new Substance();

        return fragment;
    }

    public static SubstanceChangeDialogFragment newInstance( SubstanceListManagementFragment manager, Substance substance /* to change */ ) {
        SubstanceChangeDialogFragment fragment = new SubstanceChangeDialogFragment();
        fragment.mManager = manager;
        fragment.mSubstance = substance;

        return fragment;
    }

    public SubstanceChangeDialogFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView( LayoutInflater inflater, ViewGroup container,
                              Bundle savedInstanceState ) {
        View rootView = inflater.inflate( R.layout.dlg_add_substance, container, false );

        if ( mSubstance.getId() != null ) {
            // Get substance name
            EditText substanceName = (EditText) rootView.findViewById( R.id.edt_name );

            substanceName.setText( mSubstance.getName() );

            // Change button title to "Change"
            Button btnAdd = (Button) rootView.findViewById( R.id.btn_add );

            btnAdd.setText( R.string.change );
        }

        getDialog().setTitle( "Substance Description" );

        setBtnListener( rootView, R.id.btn_add );
        setBtnListener( rootView, R.id.btn_cancel );

        return rootView;
    }

    private void setBtnListener( View rootView, int id ) {
        Button btn;
        btn = ((Button) rootView.findViewById( id ));
        btn.setOnClickListener( new View.OnClickListener()
        {
            @Override
            public void onClick( View v ) {
                switch ( v.getId() ) {
                    case R.id.btn_add:

                        EditText editText = ((EditText) getView().findViewById( R.id.edt_name ));
                        String name = editText.getText().toString();

                        if ( name.equals( "" ) ) {
                            Toast.makeText( getActivity(),
                                            "Please fill all information first.",
                                            Toast.LENGTH_SHORT ).show();  //TODO: disable "Add" button
                        } else {
                            mSubstance.setName( editText.getText().toString() );

                            if ( mSubstance.getId() == null ) {
                                mManager.objectAdd( mSubstance );   //in case this doesn't call previous Fragment because it
                                // has been destroyd, it would be necessary to place it to BACKSTACK
                            } else {
                                mManager.objectChange( mSubstance );
                            }

                            dismiss();
//                          EditStepDialogFragment.this.dismiss(); // wow! this really suprised me :D
                        }
                        break;

                    case R.id.btn_cancel:

                        dismiss();

                        break;

                    default:
                }

            }
        } );
    }

    @Override
    public void onAttach( Activity activity ) {
        super.onAttach( activity );
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mManager = null;
    }

    @Override
    public void onDismiss( DialogInterface dialog ) {
        super.onDismiss( dialog );

        Log.d( "LIFECYCLE", "SubstanceAddDialogFragment onDismiss()" );
    }

    public interface OnActionListener
    {
        public void onSubstanceAdd( Substance substance );

        public void onSubstanceChange( Substance substance );
    }

}
