package cz.exmaxx.thecookbook.db;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.os.Build;
import android.os.Environment;
import android.util.Log;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.channels.FileChannel;
import java.util.Date;
import java.util.List;

/**
 * Created by maxx on 12.8.2014.
 */
public class DaoHelper
{
    public static final String DB_NAME = "TheCookBook-DB";

    private Context mContext;

    private SQLiteDatabase mDb;
    private DaoMaster      mDaoMaster;
    private DaoSession     mDaoSession;

//    private static DaoHelper mDaoHelper;

//    private RecipeDao     mRecipeDao;
//    private SubstanceDao  mSubstanceDao;
//    private IngredientDao mIngredientDao;
//    private UnitDao       mUnitDao;
//    private StepDao       mStepDao;

    public static DaoHelper create( Context context ) {

/* Old code from times when this was create() method.
   In the end, the helper (and connection to DB) needs to be created again in most times. */

//        if ( mDaoHelper == null )
//        {
//            mDaoHelper = new DaoHelper( context );
//            return mDaoHelper;
//        }
//        else
//        {
//            return mDaoHelper;
//        }

        return new DaoHelper( context );
    }

    public DaoHelper( Context context ) {
        mContext = context;
    }

    public SQLiteDatabase initDb() {
        DaoMaster.DevOpenHelper helper = new DaoMaster.DevOpenHelper( mContext, DB_NAME, null );

        mDb = helper.getWritableDatabase();

        if ( mDb == null )
            Log.e( "DB", "open FAILED" );

        mDaoMaster = new DaoMaster( mDb );
        mDaoSession = mDaoMaster.newSession();

        return mDb;
    }

    private void arrangeDb() {
        if ( mDb == null ) {
            initDb();
        }
    }

    public SQLiteDatabase getDb() {
        arrangeDb();
        return mDb;
    }

    public DaoMaster getDaoMaster() {
        arrangeDb();
        return mDaoMaster;
    }

    public DaoSession getDaoSession() {
        arrangeDb();
        return mDaoSession;
    }

    public RecipeDao getRecipeDao() {
        arrangeDb();
        return mDaoSession.getRecipeDao();
    }

    public SubstanceDao getSubstanceDao() {
        arrangeDb();
        return mDaoSession.getSubstanceDao();
    }

    public IngredientDao getIngredientDao() {
        arrangeDb();
        return mDaoSession.getIngredientDao();
    }

    public UnitDao getUnitDao() {
        arrangeDb();
        return mDaoSession.getUnitDao();
    }

    public StepDao getStepDao() {
        arrangeDb();
        return mDaoSession.getStepDao();
    }

    public void closeDb() {
//        Log.d( "DB", "close attempt" );
//
//        if ( mDb != null )
//        {
//            Log.d( "DB", "closed" );
//
//            mDb.close();
//            mDb = null;
//        }
//        else
//            Log.d( "DB", "close FAILED - mDb already null" );
    }

    public void db_dump() {
        final String tag = "DB DUMP";

        try {
            //Recipes
            Log.d( tag, "----- Recipes -----" );

            List<Recipe> recipes = getRecipeDao().loadAll();


            for ( Recipe recipe : recipes ) {
                Log.d( tag, "Recipe: " + recipe.getId() + " " + recipe.getTitle() );

                List<Step> stepList = recipe.getStepList();

                //Steps
                List<Step> steps = getStepDao().queryBuilder()
                                               .where( StepDao.Properties.RecipeID.eq( recipe.getId() ) )
                                               .list();

                for ( Step step : steps ) {
                    Log.d( tag, " * Step: " + step.getId() + " " + step.getDescription() + " " + step.getRecipeID() );
                }

                //Ingredients
                List<Ingredient> ingredients = getIngredientDao().queryBuilder()
                                                                 .where( IngredientDao.Properties.RecipeID
                                                                                 .eq( recipe.getId() ) )
                                                                 .list();

                if ( ingredients != null && ingredients.isEmpty() == false ) {
                    List<Ingredient> ingredients_test = recipe.getIngredientList();
                }

                for ( Ingredient ingredient : ingredients ) {
                    Log.d( tag,
                           " * Ingredient: " + ingredient.getId() + " "
                                   + ingredient.getSubstance().getName() + " "
                                   + ingredient.getAmount() + " "
                                   + ingredient.getUnit().getName() );
                }

            }


            // All steps (in case some is not attached to Recipe)
            Log.d( tag, "----- Steps -----" );

            List<Step> steps = getStepDao().loadAll();

            for ( Step step : steps ) {
                Log.d( tag, "Step: " + step.getId() + " " + step.getDescription() + " " + step.getRecipeID() );
            }


            // All steps (in case some is not attached to Recipe)
            Log.d( tag, "----- Ingredients -----" );

            List<Ingredient> ingredients = getIngredientDao().loadAll();

            for ( Ingredient ingredient : ingredients ) {
                Log.d( tag,
                       "Ingredient: " + ingredient.getId() + " "
                               + ingredient.getSubstance().getName() + " "
                               + ingredient.getAmount() + " "
                               + ingredient.getUnit().getName() + " ("
                               + ingredient.getRecipeID() + ")" );
            }

        } catch ( Exception ex ) {
            Log.e( tag, "ERROR: Some shit happeeenned! Probably new version of DB? Some column not exist anymore?" );
        } finally {
            closeDb();
        }

    }

    public void purgeAllData() {
        DaoMaster.dropAllTables( getDb(), true );
    }

    public void createTestData() {
        initDb();


        DaoMaster.createAllTables( getDb(), true );


        Unit unit;

        unit = new Unit( 1L, "g" );
        getUnitDao().insert( unit );

        unit = new Unit( 2L, "kg" );
        getUnitDao().insert( unit );


        Substance substance = new Substance();

        substance = new Substance( 1L, "mrkev" );
        getSubstanceDao().insert( substance );

        substance = new Substance( 2L, "celer" );
        getSubstanceDao().insert( substance );

        substance = new Substance( 3L, "petrzel" );
        getSubstanceDao().insert( substance );


        Recipe recipe = new Recipe( 1L, "Zeleninova", new Date(), new Date(), null );
        getRecipeDao().insert( recipe );


        Ingredient ingredient = new Ingredient();

        ingredient.setRecipeID( 1L );
        ingredient.setAmount( 100 );
        ingredient.setUnitID( 1L );
        ingredient.setSubstanceID( 1l );
        getIngredientDao().insert( ingredient );

        ingredient.setRecipeID( 1L );
        ingredient.setId( null );
        ingredient.setAmount( 200 );
        ingredient.setUnitID( 1L );
        ingredient.setSubstanceID( 2l );
        getIngredientDao().insert( ingredient );


        Step step;
        step = new Step( 1L,
                         "Ocistit suroviny",
                         "Jdete do sklepa, vyberte spravne suroviny a ocistete je.",
                         5,
                         "",
                         1L );
        getStepDao().insert( step );

        step = new Step( 2L,
                         "Uvarte jidlo",
                         "Ocistene suroviny nahazte do hrnce a varte, dokud nejsou uvarene.",
                         20,
                         "",
                         1L );
        getStepDao().insert( step );


        closeDb();
    }

    public void resetDB() {
        purgeAllData();
        createTestData();
    }

    public void writeToSD( Context context ) throws IOException {
        String dbPath;

        if ( Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1 ) {
            dbPath = context.getFilesDir().getAbsolutePath().replace( "files", "databases" ) + File.separator;
        } else {
            dbPath = context.getFilesDir().getPath() + context.getPackageName() + "/databases/";
        }

        File sd = Environment.getExternalStorageDirectory();

        if ( sd.canWrite() ) {
            String currentDBPath = DB_NAME;
            String backupDBPath = "cz.exmaxx.cookbook.db";
            File currentDB = new File( dbPath, currentDBPath );
            File backupDB = new File( sd.getPath(), backupDBPath );

            if ( currentDB.exists() ) {
                FileChannel src = new FileInputStream( currentDB ).getChannel();
                FileChannel dst = new FileOutputStream( backupDB ).getChannel();
                dst.transferFrom( src, 0, src.size() );
                src.close();
                dst.close();
            }
        }
    }
}
