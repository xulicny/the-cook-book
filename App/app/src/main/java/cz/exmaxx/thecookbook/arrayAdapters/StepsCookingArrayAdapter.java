package cz.exmaxx.thecookbook.arrayAdapters;

import android.app.Activity;
import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.ArrayList;

import cz.exmaxx.thecookbook.R;
import cz.exmaxx.thecookbook.db.DaoHelper;
import cz.exmaxx.thecookbook.db.Step;
import cz.exmaxx.thecookbook.utility.TimeConvertor;

/**
 * Created by maxx on 27.8.2014.
 */
public class StepsCookingArrayAdapter extends ArrayAdapter<Step>
{

    private Context         mContext;
    private ArrayList<Step> mSteps;
    private int             mViewResource;
    private long            mActiveStepId;

    /**
     * @param context               Context.
     * @param steps                 Array <b>from which</b> is the data mapped.
     * @param connectToViewResource Resource <b>to which</b> is the data mapped.
     */
    public StepsCookingArrayAdapter( Context context, ArrayList<Step> steps, int connectToViewResource, long activeStepId ) {
        super( context, connectToViewResource, steps );

        mContext = context;
        mSteps = steps;
        mViewResource = connectToViewResource;
        mActiveStepId = activeStepId;
    }

    @Override
    public View getView( int position, View convertView, final ViewGroup parent ) {
        RelativeLayout newView = prepareView( convertView );
        fillView( position, newView );

        return newView;
    }

    private void fillView( int position, RelativeLayout newView ) {
        TextView tvTitle = (TextView) newView.findViewById( R.id.tv_title );
        TextView tvDescription = (TextView) newView.findViewById( R.id.tv_description );
        TextView tvElapsedTime = (TextView) newView.findViewById( R.id.tv_remaining_time );

        Step step = mSteps.get( position );
        DaoHelper daoHelper = DaoHelper.create( getContext() );
        step.refresh();

        tvTitle.setText( (position + 1) + ". " + step.getTitle() );
        tvDescription.setText( step.getDescription() );
        tvElapsedTime.setText( new TimeConvertor( step.getDuration(), "HH:mm:ss" ).getTimeAsStr() );

        daoHelper.closeDb();
    }

    private RelativeLayout prepareView( View convertView ) {
        RelativeLayout newView;

        if ( convertView != null ) {
            newView = (RelativeLayout) convertView;
        } else {
            newView = ((RelativeLayout) ((Activity) mContext).getLayoutInflater()
                                                             .inflate( mViewResource, null ));
        }
        return newView;
    }
}
