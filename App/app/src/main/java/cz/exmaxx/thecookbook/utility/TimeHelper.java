package cz.exmaxx.thecookbook.utility;

/**
 * Created by maxx on 31.10.2014.
 */
public class TimeHelper
{
    int mTimeInSecs;

    public TimeHelper( int timeInSecs ) {
        mTimeInSecs = timeInSecs;
    }

    public int getHours() {
        return mTimeInSecs / 3600;
    }

    public int getMinutes() {
        return (mTimeInSecs - getHours() * 3600) / 60;
    }

    public int getSeconds() {
        return (mTimeInSecs - getHours() * 3600 - getMinutes() * 60);
    }

}
