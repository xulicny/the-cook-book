package cz.exmaxx.thecookbook;

import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;

import java.io.IOException;

import cz.exmaxx.thecookbook.db.DaoHelper;
//import cz.exmaxx.thecookbook.db.Unit;
//import cz.exmaxx.thecookbook.db.UnitDao;


public class DashActivity extends ActionBarActivity
{

    private DaoHelper mDaoHelper;

    @Override
    protected void onCreate( Bundle savedInstanceState ) {
        super.onCreate( savedInstanceState );
        setContentView( R.layout.activity_dash );
        if ( savedInstanceState == null ) {
            getSupportFragmentManager().beginTransaction()
                                       .add( R.id.container, new DashFragment() )
                                       .commit();
        }

        mDaoHelper = new DaoHelper( this );

//        mDaoHelper.resetDB();
        try {
            mDaoHelper.writeToSD( this );
        } catch ( IOException e ) {
            e.printStackTrace();
        }

//        mDaoHelper.db_dump();
    }

}
