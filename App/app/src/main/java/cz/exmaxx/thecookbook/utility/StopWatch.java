package cz.exmaxx.thecookbook.utility;

import android.os.SystemClock;
import android.view.View;
import android.widget.Chronometer;

/**
 * Created by maxx on 29.10.2014.
 */
public class StopWatch
{
    public static final int SOFT = View.INVISIBLE;
    public static final int HARD = View.GONE;

    private Chronometer mChronometer;

    private long    mElapsedTimeInSecs;
    private boolean mChronoRunning;

    public StopWatch( Chronometer chronometer ) {
        mChronometer = chronometer;
    }

    public void setOnClickListener( View.OnClickListener onClickListener ) {
        mChronometer.setOnClickListener( onClickListener );
    }

    public void setOnTickListener( Chronometer.OnChronometerTickListener onTickListener ) {
        mChronometer.setOnChronometerTickListener( onTickListener );
    }

    public void start() {
        mChronometer.setBase( SystemClock.elapsedRealtime() - (mElapsedTimeInSecs * 1000) );

        mChronometer.start();

        mChronoRunning = true;
    }

    public void pause() {
        mChronometer.stop();

        updateElapsedTime();

        mChronoRunning = false;
    }

    /**
     * Small hack to force the chronometer to redraw displayed time.
     */
    public void redraw() {
        start();
        pause();
    }

    /**
     * Resets to zero.
     */
    public void reset() {
        setTime( 0 );
    }

    protected long updateElapsedTime() {
        mElapsedTimeInSecs = (SystemClock.elapsedRealtime() - mChronometer.getBase()) / 1000;

        return mElapsedTimeInSecs;
    }

    /**
     * @return Time is seconds.
     */
    public Long getTime() {
        if ( isRunning() )
            return updateElapsedTime();
        else
            return mElapsedTimeInSecs;
    }

    /**
     * Sets time and redraws stopwatch to show correct time.
     *
     * @param elapsedTimeInSecs Time in seconds.
     */
    public void setTime( long elapsedTimeInSecs ) {
        mElapsedTimeInSecs = elapsedTimeInSecs;
        redraw();
    }

    /**
     * @param visibilityMode Use constants SOFT = View.INVISIBLE (invisible but takes space in the screen and controls can align to it)
     *                       or HARD = View.GONE (disappears totally, other controlls cannot align to it)
     */
    public void hideUI( int visibilityMode ) {
        mChronometer.setVisibility( visibilityMode );
    }

    public void showUI() {
        mChronometer.setVisibility( View.VISIBLE );
    }

    public boolean isRunning() {
        return mChronoRunning;
    }

}