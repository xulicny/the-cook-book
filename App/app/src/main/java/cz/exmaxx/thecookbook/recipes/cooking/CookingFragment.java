package cz.exmaxx.thecookbook.recipes.cooking;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.Chronometer;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;

import cz.exmaxx.thecookbook.R;
import cz.exmaxx.thecookbook.arrayAdapters.StepsCookingArrayAdapter;
import cz.exmaxx.thecookbook.db.DaoHelper;
import cz.exmaxx.thecookbook.db.Recipe;
import cz.exmaxx.thecookbook.db.Step;
import cz.exmaxx.thecookbook.utility.ChangeTimeDialogFragment;
import cz.exmaxx.thecookbook.utility.StopWatch;
import cz.exmaxx.thecookbook.utility.TimeConvertor;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link CookingFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class CookingFragment extends Fragment
{
    public static final String RECIPE_ID = "RECIPE_ID";

    private Recipe    mRecipe;
    private DaoHelper mDaoHelper;
    private View      mRootView;
    private StopWatch mStopWatch;

    private StopwatchListener mStopWatchListener;
    private ListView          mListView;
    private int               mSelectedPos;
    private StepViewUpdater   mSelectedStepViewUpdater;

    public static CookingFragment newInstance( long recipeId ) {
        CookingFragment fragment = new CookingFragment();

        Bundle args = new Bundle( 1 );
        args.putLong( RECIPE_ID, recipeId );

        fragment.setArguments( args );

        return fragment;
    }

    public CookingFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView( LayoutInflater inflater, ViewGroup container,
                              Bundle savedInstanceState ) {
        Bundle args = getArguments();
        long recipeId = args.getLong( RECIPE_ID );

        mDaoHelper = DaoHelper.create( getActivity() );
        mRecipe = mDaoHelper.getRecipeDao().load( recipeId );
        mDaoHelper.closeDb();

        mRootView = inflater.inflate( R.layout.fragment_cooking, container, false );

        mStopWatchListener = new StopwatchListener();

        initStopWatch();

        initTitle();
        initDurations();
        initStepsList();
        initStartButton();

        mSelectedPos = 0;
        reloadSelectedStepUpdater();
//        updateSelectedStepView();

        return mRootView;

    }

    private void updateSelectedStepView() {mSelectedStepViewUpdater.update();}

    private void initStopWatch() {
        Chronometer chrono = (Chronometer) mRootView.findViewById( R.id.tv_remaining_time );

        mStopWatch = new StopWatch( chrono );

        mStopWatch.setOnTickListener( mStopWatchListener );

        /*mStopWatch.hideUI( StopWatch.HARD );*/
    }

    private void initStartButton() {
        final Button btn_start = (Button) mRootView.findViewById( R.id.btn_start_pause );

        btn_start.setOnClickListener( new View.OnClickListener()
        {
            @Override
            public void onClick( View v ) {
                if ( mStopWatch.isRunning() ) {
                    mStopWatch.pause();
                    btn_start.setText( R.string.start );
                } else {
                    mStopWatch.start();
                    btn_start.setText( R.string.pause );
                }
            }
        } );
    }

    private void initDurations() {
        TextView tvPrep = (TextView) mRootView.findViewById( R.id.tv_prep_time );
        TextView tvCook = (TextView) mRootView.findViewById( R.id.tv_cook_time );

        mRecipe.refresh();  //TODO: get rid of this reloading

        tvPrep.setText( mRecipe.getPrepTime() + " seconds" );
        tvCook.setText( mRecipe.getCookTime() + " seconds" );
    }

    private void initTitle() {
        TextView tvTitle = (TextView) mRootView.findViewById( R.id.tv_title );
        tvTitle.setText( mRecipe.getTitle() );
    }

    private void initStepsList() {
        mRecipe.refresh();
        ArrayList<Step> steps = (ArrayList<Step>) mRecipe.getStepList();
        mDaoHelper.closeDb();

        Step firstStep = steps.get( 0 );
        long activeStepId = 0;

        if ( firstStep != null )
            activeStepId = firstStep.getId();

        mListView = (ListView) mRootView.findViewById( R.id.lv_steps );

        mListView.setAdapter( new StepsCookingArrayAdapter( getActivity(),
                                                            steps, R.layout.list_item_step_cooking,
                                                            activeStepId ) );

        mListView.setOnItemClickListener( new AdapterView.OnItemClickListener()
        {
            @Override
            public void onItemClick( AdapterView<?> parent, View view, int position, long id ) {
                setSelectedPos( position );
                mStopWatch.reset();
            }
        } );
    }

    public Recipe getRecipe() {
        return mRecipe;
    }

    public void setRecipe( Recipe recipe ) {
        mRecipe = recipe;
    }

    public DaoHelper getDaoHelper() {
        return mDaoHelper;
    }

    public void setDaoHelper( DaoHelper daoHelper ) {
        mDaoHelper = daoHelper;
    }

    public View getRootView() {
        return mRootView;
    }

    public void setRootView( View rootView ) {
        mRootView = rootView;
    }

    public int getSelectedPos() {
        return mSelectedPos;
    }

    public void setSelectedPos( int selectedPos ) {
        if ( selectedPos >= mListView.getChildCount() )
            throw new IllegalArgumentException( "Selected step position (" + selectedPos + ") in list view is out of range." );

        mSelectedPos = selectedPos;
    }

    private void reloadSelectedStepUpdater() {
        mSelectedStepViewUpdater = new StepViewUpdater( mSelectedPos );
    }

    protected void switchToNextStep() {
        if ( getSelectedPos() + 1 < mListView.getChildCount() ) {
            setSelectedPos( getSelectedPos() + 1 );

            reloadSelectedStepUpdater();
            updateSelectedStepView();
        }
    }

    private class StopwatchListener implements View.OnClickListener, ChangeTimeDialogFragment.OnActionListener, Chronometer.OnChronometerTickListener
    {
        @Override
        public void OnTimeChanged( int seconds ) {
            mStopWatch.setTime( seconds );
        }

        @Override
        public void onChronometerTick( Chronometer chronometer ) {
            updateSelectedStepView();
        }

        @Override
        public void onClick( View v ) {
            ChangeTimeDialogFragment dialog = new ChangeTimeDialogFragment();

            dialog.setOnActionListener( this );
            dialog.setTime( mStopWatch.getTime().intValue() );
            dialog.show( getActivity().getSupportFragmentManager(),
                         "change_time_dialog" );
        }


    }

    protected class StepViewUpdater
    {
        int mStepViewPos;

        public StepViewUpdater( int stepViewPos ) {
            mStepViewPos = stepViewPos;
        }

        public void update() {
            updateVisuals();
            updateTime();
        }

        public void updateVisuals() {
            if ( isSelected() )
                getStepView().setBackgroundColor( getResources().getColor( R.color.step_just_cooking ) );
            else
                getStepView().setBackgroundColor( getResources().getColor( R.color.step_cooked ) );
        }

        public void updateTime() {
            boolean switchToNextStep = false;

            View stepView = getStepView();
            Step step = getStepObject();
            TextView tvTime = ((TextView) stepView.findViewById( R.id.tv_remaining_time ));

            int timeDuration = step.getDurationOfPrevStepsPlusOwn();
            int timeElapsed = mStopWatch.getTime().intValue();
            int timeRemaining = timeDuration - timeElapsed;

            if ( timeRemaining < 0 ) {
                timeRemaining = 0;
                switchToNextStep = true;
            }

            tvTime.setText( new TimeConvertor( timeRemaining, "HH:mm:ss" ).getTimeAsStr() );

            if ( switchToNextStep ) {
                switchToNextStep();
                updateVisuals();
            }
        }

        private View getStepView() {
            return mListView.getChildAt( mStepViewPos );
        }

        private Step getStepObject() {
            return (Step) mListView.getAdapter().getItem( mStepViewPos );
        }

        private boolean isSelected() {
            return mStepViewPos == mSelectedPos;
        }

    }

}
