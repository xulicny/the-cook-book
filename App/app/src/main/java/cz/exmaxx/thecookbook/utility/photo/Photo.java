package cz.exmaxx.thecookbook.utility.photo;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.media.ExifInterface;
import android.net.Uri;
import android.util.Log;

import java.io.File;
import java.io.IOException;

/**
 * Created by maxx on 19/11/14.
 */
public class Photo
{
    String mFilePath;
    Bitmap mBitmap;
    int    mOrientation;

    public static Photo createFromFile( String mFilePath ) {

        Photo photo = null;

        if ( new File( mFilePath ).exists() )
            photo = new Photo( mFilePath );
        else
            Log.e( "Photo", "File \"" + mFilePath + "\" doesn't exist." );

        return photo;
    }

    public static Bitmap resizeBitmap( Bitmap bitmap, int resizedWidth, int resizedHeight ) {
        return Bitmap.createBitmap( bitmap,
                                    0,
                                    0,
                                    resizedWidth,
                                    resizedHeight );
    }

    public static Bitmap rotateBitmap( Bitmap bitmap, float rotationDegrees ) {
        Matrix matrix = new Matrix();
        matrix.postRotate( rotationDegrees );

        return Bitmap.createBitmap( bitmap,
                                    0,
                                    0,
                                    bitmap.getWidth(),
                                    bitmap.getHeight(),
                                    matrix,
                                    true );
    }

    private Photo( String filePath ) {
        mFilePath = filePath;
    }

    private Photo() {
    }

    public String getFilePath() {
        return mFilePath;
    }

    public Bitmap getBitmap() {
        if ( mBitmap == null ) {
            mBitmap = BitmapFactory.decodeFile( mFilePath );
        }

        return mBitmap;
    }

    public Bitmap getBitmap( int resizedWidth, int resizedHeight ) {
        return resizeBitmap( getBitmap(), resizedWidth, resizedHeight );
    }

    public Bitmap getBitmap( float rotationDegrees ) {
        return rotateBitmap( getBitmap(), rotationDegrees );
    }

    public Bitmap getBitmap( int resizedWidth, int resizedHeight, float rotationDegrees ) {
        if ( rotationDegrees == 0 )
            return getBitmap( resizedWidth, resizedHeight );
        else
            return rotateBitmap( getBitmap( resizedWidth, resizedHeight ), rotationDegrees );
    }

    public int getOrientation() {
        if ( mOrientation == ExifInterface.ORIENTATION_UNDEFINED ) {
            ExifInterface exif = null;
            try {
                exif = new ExifInterface( mFilePath );
            } catch ( IOException e ) {
                e.printStackTrace();
            }

            mOrientation = exif.getAttributeInt( ExifInterface.TAG_ORIENTATION, ExifInterface.ORIENTATION_NORMAL );
        }

        return mOrientation;
    }

    public void addToGallery( Activity activity ) {
        File f = new File( mFilePath );
        Uri contentUri = Uri.fromFile( f );

        Intent mediaScanIntent = new Intent( Intent.ACTION_MEDIA_SCANNER_SCAN_FILE );

        mediaScanIntent.setData( contentUri );
        activity.sendBroadcast( mediaScanIntent );
    }
}
