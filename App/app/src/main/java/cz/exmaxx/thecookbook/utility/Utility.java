package cz.exmaxx.thecookbook.utility;

import android.content.Context;
import android.widget.Toast;

/**
 * Created by maxx on 13.11.2014.
 */
public class Utility
{
    public static void showToast( String message, Context context ) {
        showToast( message, Toast.LENGTH_SHORT, context );
    }

    public static void showToast( String message, int duration, Context context ) {

        Toast.makeText( context, message, duration ).show();
    }
}
