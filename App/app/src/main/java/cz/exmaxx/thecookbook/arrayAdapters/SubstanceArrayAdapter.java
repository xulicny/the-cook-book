package cz.exmaxx.thecookbook.arrayAdapters;

import android.app.Activity;
import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.List;

import cz.exmaxx.thecookbook.R;
import cz.exmaxx.thecookbook.db.Substance;
import cz.exmaxx.thecookbook.listManagement.SubstanceListManagementFragment;

/**
 * Created by maxx on 27.8.2014.
 */
public class SubstanceArrayAdapter extends ArrayAdapter<Substance>
{
    public interface OnBtnActionListener
    {
        void onChangeClicked( Long substanceId );

        void onRemoveClicked( Long substanceId );
    }

    private Context                         mContext;
    private List<Substance>                 mSubtances;
    private int                             mViewResource;
    private SubstanceListManagementFragment mFragment;

    public SubstanceArrayAdapter( Context context, List<Substance> substances, int connectToViewResource, SubstanceListManagementFragment fragment )
    {
        super( context, connectToViewResource, substances );

        mContext = context;
        mSubtances = substances;
        mViewResource = connectToViewResource;
        mFragment = fragment;
    }

    @Override
    public View getView( int position, View convertView, final ViewGroup parent )
    {
        final Substance substance = mSubtances.get( position );

        RelativeLayout newView;

        if ( convertView != null )
        {
            newView = (RelativeLayout) convertView;
        }
        else
        {
            newView = ((RelativeLayout) ((Activity) mContext).getLayoutInflater()
                                                             .inflate( mViewResource, null ));
        }

        TextView tv1 = (TextView) newView.findViewById( android.R.id.text1 );
        TextView tv2 = (TextView) newView.findViewById( android.R.id.text2 );

        tv1.setText( substance.getName() );
        tv2.setText( "(ID: " + substance.getId() + ")" );

        Button btnChange = (Button) newView.findViewById( R.id.btn_change );
        Button btnRemove = (Button) newView.findViewById( R.id.btn_remove );

        btnChange.setOnClickListener( new View.OnClickListener()
        {
            @Override
            public void onClick( View v )
            {
                mFragment.onChangeClicked( substance.getId() );
            }
        } );

        btnRemove.setOnClickListener( new View.OnClickListener()
        {
            @Override
            public void onClick( View v )
            {
                mFragment.onRemoveClicked( substance.getId() );
            }
        } );

        return newView;
    }
}
