package cz.exmaxx.thecookbook.listManagement;

import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.List;

import cz.exmaxx.thecookbook.R;
import cz.exmaxx.thecookbook.db.DaoHelper;

public abstract class ListManagementFragment extends Fragment
{
    protected DaoHelper mDaoHelper;
    protected ListView  mListView;
    protected ArrayList<Long> mIDs = new ArrayList<Long>();   //keeping this field so that I can invalidate adapter later

    List mList;

    protected static void logCastError( Object o, ClassCastException e )
    {
        Log.e( "CAST EXCEPTION", "Exception: " + e.toString() + " Class: " + o.getClass().getName() );

        e.printStackTrace();
    }

    public ListManagementFragment()
    {
        // Required empty public constructor
    }

    //TODO: blbe navrzeno - neni intuitivni - viz comment v UnitListManagementFragment
    abstract void createList();

    //TODO: blbe navrzeno - neni intuitivni - viz comment v UnitListManagementFragment
    abstract void populateList();

    abstract void onInsertClicked();

    abstract void objectAdd( Object o );

    abstract void objectChange( Object o );

//    abstract void onShowDetailClicked();

    abstract void objectRemove( Object o );

    abstract void setAdapter();


    @Override
    public void onAttach( Activity activity )
    {
        super.onAttach( activity );

        mDaoHelper = new DaoHelper( activity );
    }

    @Override
    public void onCreate( Bundle savedInstanceState )
    {
        super.onCreate( savedInstanceState );
    }

    @Override
    public View onCreateView( LayoutInflater inflater, ViewGroup container,
                              Bundle savedInstanceState )
    {
        // Inflate the layout for this fragment
        final View rootView = inflater.inflate( R.layout.fragment_list_management, container, false );

        // Init listview
        mListView = ((ListView) rootView.findViewById( R.id.managedList ));

        // Set adapter
        createList();
        setAdapter();
        refreshList();

        // Handle button clicks
        Button btn;

        int[] btnIDs = {R.id.btn_new /* add more if needed */};

        for ( final int btnID : btnIDs )
        {
            btn = ((Button) rootView.findViewById( btnID ));
            btn.setOnClickListener( new View.OnClickListener()
            {
                @Override
                public void onClick( View v )
                {
                    switch ( btnID )
                    {
                        case R.id.btn_new:
                            onInsertClicked();
                            break;
                    }
                }
            } );
        }

        return rootView;
    }

    @Override
    public void onDetach()
    {
        super.onDetach();
    }

    protected void refreshList()
    {
        // Update IDs
        populateList();

        // Invalidate List View
        ((BaseAdapter) mListView.getAdapter()).notifyDataSetChanged();
    }

}
