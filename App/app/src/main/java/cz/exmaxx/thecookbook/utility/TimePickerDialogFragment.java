package cz.exmaxx.thecookbook.utility;

import android.app.Dialog;
import android.app.TimePickerDialog;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.widget.TimePicker;

import java.util.Calendar;

/**
 * Created by maxx on 31.10.2014.
 */
public class TimePickerDialogFragment extends DialogFragment implements TimePickerDialog.OnTimeSetListener
{
    private int mTimeInSecs;

    public static void newInstance( int timeInSecs ) {
        TimePickerDialogFragment dialog = new TimePickerDialogFragment();
        dialog.mTimeInSecs = timeInSecs;
    }

    public TimePickerDialogFragment() {
    }

    @Override
    public Dialog onCreateDialog( Bundle savedInstanceState ) {
        TimeHelper timeConvertor = new TimeHelper( mTimeInSecs );

        Calendar calendar = Calendar.getInstance();

        return new TimePickerDialog( getActivity(), this, timeConvertor.getHours(), timeConvertor.getMinutes(), true );
    }

    @Override
    public void onTimeSet( TimePicker view, int hourOfDay, int minute ) {

    }
}
