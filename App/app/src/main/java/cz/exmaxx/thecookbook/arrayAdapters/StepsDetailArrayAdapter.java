package cz.exmaxx.thecookbook.arrayAdapters;

import android.app.Activity;
import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.ArrayList;

import cz.exmaxx.thecookbook.R;
import cz.exmaxx.thecookbook.db.DaoHelper;
import cz.exmaxx.thecookbook.db.Step;
import cz.exmaxx.thecookbook.utility.TimeConvertor;

/**
 * Created by maxx on 27.8.2014.
 */
public class StepsDetailArrayAdapter extends ArrayAdapter<Step>
{

    private Context         mContext;
    private ArrayList<Step> mSteps;
    private int             mViewResource;

    /**
     * @param context               Context.
     * @param steps                 Array <b>from which</b> is the data mapped.
     * @param connectToViewResource Resource <b>to which</b> is the data mapped.
     */
    public StepsDetailArrayAdapter( Context context, ArrayList<Step> steps, int connectToViewResource ) {
        super( context, connectToViewResource, steps );

        mContext = context;
        mSteps = steps;
        mViewResource = connectToViewResource;
    }

    @Override
    public View getView( int position, View convertView, final ViewGroup parent ) {
        Step step = mSteps.get( position );

        RelativeLayout newView;

        if ( convertView != null ) {
            newView = (RelativeLayout) convertView;
        } else {
            newView = ((RelativeLayout) ((Activity) mContext).getLayoutInflater()
                                                             .inflate( mViewResource, null ));
        }

        TextView title = (TextView) newView.findViewById( R.id.tv_title );
        TextView description = (TextView) newView.findViewById( R.id.tv_description );
        TextView time = (TextView) newView.findViewById( R.id.tv_time );

        DaoHelper daoHelper = DaoHelper.create( getContext() );
        step.refresh();

        title.setText( (position + 1) + " // " + step.getTitle() );
        description.setText( step.getDescription() );

        TimeConvertor timeFormatter = new TimeConvertor( step.getDuration(), "HH:mm:ss" );
        time.setText( timeFormatter.getTimeAsStr() );

        daoHelper.closeDb();

        return newView;
    }
}
