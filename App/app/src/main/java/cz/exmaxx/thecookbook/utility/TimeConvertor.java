package cz.exmaxx.thecookbook.utility;

import android.util.Log;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.TimeZone;

/**
 * Created by maxx on 25.10.2014.
 */
public class TimeConvertor
{
    public static final String STANDARD_FORMAT = "HH:mm:ss";

    String mTimeAsStr;
    int    mTimeInSecs;

    String mFormat;
    private boolean mIsCalculated;

    public TimeConvertor( int timeInSecs ) {
        mTimeInSecs = timeInSecs;
        mFormat = STANDARD_FORMAT;
    }

    public TimeConvertor( int timeIsSecs, String format ) {
        mTimeInSecs = timeIsSecs;
        mFormat = format;
    }

    public TimeConvertor( String timeAsStr, String format ) {
        mTimeAsStr = timeAsStr;
        mFormat = format;
    }

    public String getTimeAsStr() {
        if ( !mIsCalculated )
            calculateTimeStr();

        return mTimeAsStr;
    }

    public int getTimeInSecs() {
        if ( !mIsCalculated )
            calculateTimeInt();

        return mTimeInSecs;
    }

    public void setTime( String timeAsStr ) {
        mTimeAsStr = timeAsStr;
        mIsCalculated = false;
    }

    public void setTime( int timeInSecs ) {
        mTimeInSecs = timeInSecs;
        mIsCalculated = false;

    }

    protected void calculateTimeStr() {
        Date dateTime = new Date( mTimeInSecs * 1000 );
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat( mFormat );

        simpleDateFormat.setTimeZone( TimeZone.getTimeZone( "UTC" ) );

        mTimeAsStr = simpleDateFormat.format( dateTime );
    }

    protected void calculateTimeInt() {
        Calendar calendar = new GregorianCalendar();
        Date dateTime;

        try {
            dateTime = new SimpleDateFormat( mFormat ).parse( mTimeAsStr );
            calendar.setTime( dateTime );
        } catch ( ParseException e ) {
            e.printStackTrace();
            Log.e( "onStepAdd", e.toString() );
        }

        mTimeInSecs = calendar.get( Calendar.HOUR ) * 3600 +
                calendar.get( Calendar.MINUTE ) * 60 +
                calendar.get( Calendar.SECOND );
    }
}
