package cz.exmaxx.thecookbook.recipes.cooking;

import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;

import cz.exmaxx.thecookbook.R;

public class CookingActivity extends ActionBarActivity
{
    public static final String RECIPE_ID = "RECIPE_ID";

    @Override
    protected void onCreate( Bundle savedInstanceState )
    {
        super.onCreate( savedInstanceState );
        setContentView( R.layout.activity_cooking );
        if ( savedInstanceState == null )
        {
            long recipeId = getIntent().getLongExtra( RECIPE_ID, 0 );

            CookingFragment cookingFragment = CookingFragment.newInstance( recipeId );

            getSupportFragmentManager().beginTransaction()
                                       .add( R.id.container, cookingFragment )
                                       .commit();
        }
    }

    @Override
    public boolean onCreateOptionsMenu( Menu menu )
    {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate( R.menu.cooking, menu );
        return true;
    }

    @Override
    public boolean onOptionsItemSelected( MenuItem item )
    {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if ( id == R.id.action_settings )
        {
            return true;
        }
        return super.onOptionsItemSelected( item );
    }

}
