package cz.exmaxx.thecookbook.utility;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.widget.EditText;

public class ChangeTimeDialogFragment extends DialogFragment
{
    public static final String TIME_FORMAT = "HH:mm:ss";

    private int              mTime; // in seconds
    private OnActionListener mOnActionListener;

    public ChangeTimeDialogFragment() {
    }

    @Override
    public Dialog onCreateDialog( Bundle savedInstanceState ) {

        TimeConvertor timeFormatter = new TimeConvertor( mTime, TIME_FORMAT );

        final EditText editTime = new EditText( getActivity() );
        editTime.setText( timeFormatter.getTimeAsStr() );

        AlertDialog.Builder builder = new AlertDialog.Builder( getActivity() );

        builder.setTitle( "Set Time" )
               .setView( editTime )
               .setPositiveButton( android.R.string.ok, new DialogInterface.OnClickListener()
               {
                   @Override
                   public void onClick( DialogInterface dialog, int which ) {
                       TimeConvertor timeFormatter = new TimeConvertor( editTime.getText().toString(), TIME_FORMAT );
                       mOnActionListener.OnTimeChanged( timeFormatter.getTimeInSecs() );
                       dismiss();
                   }
               } )
               .setNegativeButton( android.R.string.cancel, new DialogInterface.OnClickListener()
               {
                   @Override
                   public void onClick( DialogInterface dialog, int which ) {
                       dismiss();
                   }
               } );

        return builder.create();
    }

    public OnActionListener getOnActionListener() {
        return mOnActionListener;
    }

    public void setOnActionListener( OnActionListener onActionListener ) {
        mOnActionListener = onActionListener;
    }

    public int getTime() {
        return mTime;
    }

    public void setTime( int timeInSecs ) {
        mTime = timeInSecs;
    }

    public void setTime( String time, String format ) {
        TimeConvertor timeFormatter = new TimeConvertor( time, format );
        setTime( timeFormatter.getTimeInSecs() );
    }

    public static interface OnActionListener
    {
        void OnTimeChanged( int seconds );
    }
}
