package cz.exmaxx.thecookbook.utility.photo;


import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.provider.MediaStore;
import android.support.v4.app.Fragment;
import android.util.Log;

import java.io.File;
import java.io.IOException;

import cz.exmaxx.thecookbook.utility.Utility;

/**
 * Created by maxx on 21/11/14.
 */
public class Camera
{
    public static final int REQUEST_IMAGE_CAPTURE = 1;

    final Fragment mFragment;

    String mPhotoPath;

    public Camera( Fragment fragment ) {
        mFragment = fragment;
    }


    public boolean isCameraAvailable() {
        if ( !mFragment.getActivity().getPackageManager().hasSystemFeature( PackageManager.FEATURE_CAMERA ) ) {
            return false;
        } else
            return true;
    }

    public void startCameraActivity() {
        if ( !isCameraAvailable() ) {
            Utility.showToast( "No camera detected :(", mFragment.getActivity() );
            return;
        }


        Intent takePictureIntent = new Intent( MediaStore.ACTION_IMAGE_CAPTURE );

        boolean isActionPossible = takePictureIntent
                .resolveActivity( mFragment.getActivity().getPackageManager() ) != null;

        if ( isActionPossible ) {
            File photoFile;

            try {
                photoFile = PhotoHelper.createImageFile();
            } catch ( IOException ex ) {
                Utility.showToast( "Unable to initialize file.", mFragment.getActivity() );
                Log.e( "PHOTO", ex.getMessage() );
                return;
            }

            if ( photoFile != null ) {
                mPhotoPath = photoFile.getAbsolutePath();

                takePictureIntent.putExtra( MediaStore.EXTRA_OUTPUT, Uri.fromFile( photoFile ) );
                mFragment.startActivityForResult( takePictureIntent, REQUEST_IMAGE_CAPTURE );
            }
        }
    }

    public void onPictureTaken() {

    }

    public Photo getPhoto() {
        return Photo.createFromFile( mPhotoPath );
    }

    public String getPhotoPath() {
        return mPhotoPath;
    }

}
