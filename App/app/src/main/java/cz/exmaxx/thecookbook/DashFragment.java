package cz.exmaxx.thecookbook;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.Collections;

import cz.exmaxx.thecookbook.arrayAdapters.RecipeArrayAdapter;
import cz.exmaxx.thecookbook.db.DaoHelper;
import cz.exmaxx.thecookbook.db.Recipe;
import cz.exmaxx.thecookbook.listManagement.SubstanceListManagementFragment;
import cz.exmaxx.thecookbook.listManagement.UnitListManagementFragment;
import cz.exmaxx.thecookbook.recipes.detail.RecipeDetailActivity;
import cz.exmaxx.thecookbook.recipes.edit.RecipeEditActivity;

/**
 * Created by maxx on 9.8.2014.
 */
public class DashFragment extends Fragment
{

    //    private OnActionListener mListener;
    private DaoHelper mDaoHelper;

    public DashFragment()
    {

    }

    @Override
    public void onAttach( Activity activity )
    {
        super.onAttach( activity );

        mDaoHelper = new DaoHelper( getActivity() );
    }

    @Override
    public View onCreateView( LayoutInflater inflater, ViewGroup container,
                              Bundle savedInstanceState )
    {
//        mDaoHelper.resetDB();


        // Create view
        View rootView = inflater.inflate( R.layout.fragment_dash, container, false );

        // Set listener
        Button btn;

        int[] btnIDs = {R.id.btn_new, R.id.btn_units_maintain, R.id.btn_substances_maintain};

        for ( int btnID : btnIDs )
        {
            btn = ((Button) rootView.findViewById( btnID ));
            btn.setOnClickListener( new View.OnClickListener()
            {
                @Override
                public void onClick( View v )
                {
                    onBtnClick( v );
                }
            } );
        }

        // Refresh list of recipes
        refreshListRecipes( rootView );

        return rootView;
    }

    @Override
    public void onResume()
    {
        super.onResume();
        Log.d( "LIFECYCLE", "DashFragment onResume()" );

        refreshListRecipes( getView() );
    }

    public void onBtnClick( View v )
    {
        switch ( v.getId() )
        {

            case R.id.btn_new:
                Intent i = new Intent( getActivity(), RecipeEditActivity.class );
                startActivity( i );

                break;

            case R.id.btn_units_maintain:
                UnitListManagementFragment unitListFragment = new UnitListManagementFragment();

                getActivity().getSupportFragmentManager()
                             .beginTransaction()
                             .replace( R.id.container, unitListFragment )
                             .addToBackStack( null )
                             .commit();

                break;

            case R.id.btn_substances_maintain:
                SubstanceListManagementFragment substanceListManagFragment = new SubstanceListManagementFragment();

                getActivity().getSupportFragmentManager()
                             .beginTransaction()
                             .replace( R.id.container, substanceListManagFragment )
                             .addToBackStack( null )
                             .commit();

                break;
        }

    }


    protected void refreshListRecipes( View rootView )
    {
        ArrayList<String> titles = new ArrayList<String>();
        ArrayList<Recipe> recipes = (ArrayList<Recipe>) mDaoHelper.getRecipeDao().loadAll();
        mDaoHelper.closeDb();

        Collections.reverse( recipes );

        ListView listView = (ListView) rootView.findViewById( R.id.recipes );

        listView.setAdapter( new RecipeArrayAdapter( getActivity(), recipes, R.layout.list_item_recipe ) );

        listView.setOnItemClickListener( new AdapterView.OnItemClickListener()
        {
            @Override
            public void onItemClick( AdapterView<?> parent, View view, int position, long id )
            {
                Recipe recipe = (Recipe) parent.getAdapter().getItem( position );

                Intent intent = new Intent( getActivity(), RecipeDetailActivity.class );
                intent.putExtra( RecipeDetailActivity.RECIPE_ID, recipe.getId() );

                startActivity( intent );
            }
        } );
    }

}

