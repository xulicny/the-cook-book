package cz.exmaxx.thecookbook.arrayAdapters;

import android.app.Activity;
import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.text.DateFormat;
import java.util.ArrayList;
import java.util.Date;

import cz.exmaxx.thecookbook.R;
import cz.exmaxx.thecookbook.db.Recipe;

/**
 * Created by maxx on 27.8.2014.
 */
public class RecipeArrayAdapter extends ArrayAdapter<Recipe>
{

    private Context           mContext;
    private ArrayList<Recipe> mRecipes;
    private int               mViewResource;

    /**
     * @param context               Context.
     * @param recipes               Array <b>from which</b> is the data mapped.
     * @param connectToViewResource Resource <b>to which</b> is the data mapped.
     */
    public RecipeArrayAdapter( Context context, ArrayList<Recipe> recipes, int connectToViewResource )
    {
        super( context, connectToViewResource, recipes );

        mContext = context;
        mRecipes = recipes;
        mViewResource = connectToViewResource;
    }

    @Override
    public View getView( int position, View convertView, final ViewGroup parent )
    {
        RelativeLayout newView = chooseView( convertView );

        setTexts( position, newView );

        return newView;
    }

    protected RelativeLayout chooseView( View convertView )
    {
        RelativeLayout newView;

        if ( convertView != null )
        {
            newView = (RelativeLayout) convertView;
        }
        else
        {
            newView = ((RelativeLayout) ((Activity) mContext).getLayoutInflater()
                                                             .inflate( mViewResource, null ));
        }

        return newView;
    }

    protected void setTexts( int position, RelativeLayout newView )
    {
        final Recipe recipe = mRecipes.get( position );

        TextView tvTitle = (TextView) newView.findViewById( R.id.tv_title );
        TextView tvId = (TextView) newView.findViewById( R.id.id );
        TextView tvDate = (TextView) newView.findViewById( R.id.date );

        tvTitle.setText( recipe.getTitle() );
        tvId.setText( "(ID: " + recipe.getId() + ")" );
        tvDate.setText( getFormattedDate( recipe.getCreationDate() ) );
    }

    private String getFormattedDate( Date date )
    {
        DateFormat dateFormat = DateFormat.getDateInstance();
        return dateFormat.format( date );
    }
}
