package cz.exmaxx.generator.db;

import java.util.List;
import java.util.zip.CheckedInputStream;

import de.greenrobot.daogenerator.DaoGenerator;
import de.greenrobot.daogenerator.Entity;
import de.greenrobot.daogenerator.Property;
import de.greenrobot.daogenerator.Schema;
import de.greenrobot.daogenerator.ToOne;

/**
 * Created by maxx on 7.8.2014.
 */
public class TheCookbookDAOGenerator
{
    public static void main( String[] args ) throws Exception
    {
        Property recipeID;
        Property unitID;
        Property substanceID;
        Property ingredientID;

        Schema schema = new Schema( 1, "cz.exmaxx.thecookbook.db" );
        schema.enableKeepSectionsByDefault();

        /* Substance (or Material? nah, too SAPish ;) */
        Entity substance = schema.addEntity( "Substance" );
        substance.addIdProperty();
        substance.addStringProperty( "name" );

        /* Unit */
        Entity unit = schema.addEntity( "Unit" );
        unit.addIdProperty().getProperty();
        unit.addStringProperty( "name" );

        /* Ingredient */
        Entity ingredient = schema.addEntity( "Ingredient" );
        ingredient.addIdProperty();
        ingredient.addFloatProperty( "amount" ).notNull();
        recipeID = ingredient.addLongProperty( "recipeID" ).notNull().getProperty();
        unitID = ingredient.addLongProperty( "unitID" ).notNull().getProperty();
        substanceID = ingredient.addLongProperty( "substanceID" ).notNull().getProperty();

        ingredient.addToOne( unit, unitID );
        ingredient.addToOne( substance, substanceID );

//        /* Tag */
//        Entity tag = schema.addEntity( "Tag" );
//        tag.addIdProperty();
//        tag.addStringProperty( "name" );
//        //collection relation added bellow

//        /* Variant Group */
//        Entity variantGroup = schema.addEntity( "Variant_Group" );
//        variantGroup.addIdProperty();

        /* Recipe */
        Entity recipe = schema.addEntity( "Recipe" );
        recipe.addIdProperty().primaryKeyAsc().autoincrement();
        recipe.addStringProperty( "title" );
        recipe.addDateProperty( "creationDate" );
        recipe.addDateProperty( "lastEditDate" );
        recipe.addStringProperty( "thumbnailPath" );

//        Property variantGroupID = recipe.addLongProperty( "variantGroupID" ).getProperty();
//        ingredientID = recipe.addLongProperty( "ingredientID" ).getProperty();

//        recipe.addToOne( variantGroup, variantGroupID );
        recipe.addToMany( ingredient, recipeID );
        //collection relation added bellow

        /* Step */
        Entity step = schema.addEntity( "Step" );
        step.addIdProperty();
        step.addStringProperty( "title" );
        step.addStringProperty( "description" );
        step.addIntProperty( "duration" );
        step.addStringProperty( "picturePath" );
        recipeID = step.addLongProperty( "recipeID" ).notNull().index().getProperty();

        ToOne toOne = step.addToOne( recipe, recipeID );
        Property[] fkProperties = toOne.getFkProperties();
        recipe.addToMany( step, recipeID );

//        /* Collection */
//        Entity collection = schema.addEntity( "Collection" );
//        Property recipeID = collection.addLongProperty( "Recipe_ID" ).notNull().getProperty();
//        Property tagID = collection.addLongProperty( "Tag_ID" ).notNull().getProperty();
//
//        collection.addToOne( recipe, recipeID );
//        collection.addToOne( tag, tagID );
//
//        //todo: is the following needed?
//        recipe.addToMany( collection, recipeID );
//        tag.addToMany( collection, tagID );

//        /* Addon */
//        Entity addon = schema.addEntity( "Addon" );
//        Property baseRecipeID = addon.addLongProperty( "Base_ID" ).notNull().getProperty();
//        Property componentID = addon.addLongProperty( "Component_ID" ).notNull().getProperty();
//
//        addon.addToOne( recipe, baseRecipeID );
//        addon.addToOne( recipe, componentID );
//
//        //todo: is the following needed?
//        recipe.addToMany( addon, baseRecipeID, "usedAsBase" );
//        recipe.addToMany( addon, componentID, "usedAsComponent" );


        /* GENERATE */

        new DaoGenerator().generateAll( schema, "../App/app/src/main/java" );

    }

}
