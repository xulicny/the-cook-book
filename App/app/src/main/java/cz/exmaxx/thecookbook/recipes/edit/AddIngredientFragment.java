package cz.exmaxx.thecookbook.recipes.edit;

import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import cz.exmaxx.thecookbook.R;
import cz.exmaxx.thecookbook.db.DaoHelper;
import cz.exmaxx.thecookbook.db.Substance;
import cz.exmaxx.thecookbook.db.SubstanceDao;
import cz.exmaxx.thecookbook.db.Unit;
import cz.exmaxx.thecookbook.db.UnitDao;
import de.greenrobot.dao.query.QueryBuilder;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link cz.exmaxx.thecookbook.recipes.edit.AddIngredientFragment.OnActionListener} interface
 * to handle interaction events.
 * Use the {@link AddIngredientFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class AddIngredientFragment extends DialogFragment
{
    public interface OnActionListener
    {
        public void onAddIngredient( Substance substance, Unit unit, float amount );
    }

    protected DaoHelper            mDaoHelper;
    protected OnActionListener     mListener;
    protected ListView             mSubstancesListView;
    protected List<String>         mSubstanceNames;
    protected EditText             mSubstanceCtrl;
    protected AutoCompleteTextView mUnitCtrl;
    protected EditText             mAmountCtrl;

    public static AddIngredientFragment newInstance( OnActionListener listener )
    {
        AddIngredientFragment fragment = new AddIngredientFragment();
        fragment.mListener = listener;

        return fragment;
    }

    public AddIngredientFragment()
    {
        // Required empty public constructor
    }

    @Override
    public void onAttach( Activity activity )
    {
        super.onAttach( activity );

        mDaoHelper = new DaoHelper( activity );
    }

    @Override
    public View onCreateView( LayoutInflater inflater, ViewGroup container,
                              Bundle savedInstanceState )
    {
        View rootView = inflater.inflate( R.layout.dlg_add_ingredient, container, false );

        getDialog().setTitle( "Ingredient Detail" );


        initSubstancesList( rootView );

        initSubstanceEdit( rootView );
        initUnitsEdit( rootView );
        initAmountEdit( rootView );

        initButtons( rootView );


        return rootView;

        //TODO: examine FrameLayout in deatail and experiment with it
    }

    private void initSubstancesList( View rootView )
    {
        mSubstancesListView = (ListView) rootView.findViewById( R.id.list_substances );

        mSubstancesListView.setOnItemClickListener( new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick( AdapterView<?> parent, View view, int position, long id )
            {
                TextView textView = (TextView) view.findViewById( android.R.id.text1 );

                mSubstanceCtrl.setText( textView.getText().toString() );
            }
        } );

        refreshList();
    }

    protected void initButtons( View rootView )
    {
        setBtnListener( rootView, R.id.btn_add );
        setBtnListener( rootView, R.id.btn_cancel );
    }

    protected void setBtnListener( View rootView, int id )
    {
        Button btn;
        btn = ((Button) rootView.findViewById( id ));
        btn.setOnClickListener( new View.OnClickListener()
        {
            @Override
            public void onClick( View v )
            {
                switch ( v.getId() )
                {
                    case R.id.btn_add:
                        onAddPressed();
                        break;

                    case R.id.btn_cancel:
                        dismiss();
                        break;

                    default:
                }
            }
        } );
    }

    protected void initAmountEdit( View rootView )
    {
        mAmountCtrl = (EditText) rootView.findViewById( R.id.edt_amount );
    }

    protected void initUnitsEdit( View rootView )
    {
        mUnitCtrl = (AutoCompleteTextView) rootView.findViewById( R.id.edt_unit );

        List<Unit> units = mDaoHelper.getUnitDao().loadAll();
        ArrayList<String> unitNames = new ArrayList<String>();

        for ( Unit unit : units )
        {
            unitNames.add( unit.getName() );
        }

        mUnitCtrl.setAdapter( new ArrayAdapter<String>( getActivity(),
                                                        android.R.layout.simple_list_item_1,
                                                        unitNames ) );
    }

    protected void initSubstanceEdit( View rootView )
    {
        mSubstanceCtrl = (EditText) rootView.findViewById( R.id.edt_substance_name );

        mSubstanceCtrl.addTextChangedListener( new TextWatcher()
        {
            @Override
            public void beforeTextChanged( CharSequence s, int start, int count, int after )
            {

            }

            @Override
            public void onTextChanged( CharSequence s, int start, int before, int count )
            {

            }

            @Override
            public void afterTextChanged( Editable s )
            {
                refreshList( s.toString() );
            }
        } );
    }

    protected void refreshList()
    {
        refreshList( null );
    }

    protected void refreshList( String filter )
    {
        List<Substance> substances;

        if ( filter == null || filter.equals( "" ) )
        {
            substances = mDaoHelper.getSubstanceDao().loadAll();
            mDaoHelper.closeDb();
        }
        else
        {
            QueryBuilder<Substance> qb = mDaoHelper.getSubstanceDao().queryBuilder();

            substances = qb.where( SubstanceDao.Properties.Name.like( filter + "%" ) )
                           .list(); //TODO: greendao, this should work
            mDaoHelper.closeDb();
        }


        if ( mSubstanceNames == null )
        {
            mSubstanceNames = new ArrayList<String>();
        }
        else
        {
            mSubstanceNames.clear();
        }

        for ( Substance substance : substances )
        {
            mSubstanceNames.add( substance.getName() );
        }

        ArrayAdapter<String> adapter = (ArrayAdapter<String>) mSubstancesListView.getAdapter();

        if ( adapter == null )
        {
            mSubstancesListView.setAdapter( new ArrayAdapter<String>( getActivity(),
                                                            android.R.layout.simple_list_item_1,
                                                            android.R.id.text1,
                                                            mSubstanceNames ) );
        }
        else
        {
            adapter.notifyDataSetChanged();
        }
    }

    public void onAddPressed()
    {
        if ( mListener != null )
        {
            List<Substance> substances = mDaoHelper.getSubstanceDao().queryBuilder()
                                                   .where( SubstanceDao.Properties.Name
                                                                   .eq( mSubstanceCtrl.getText().toString() ) ).list();

            List<Unit> units = mDaoHelper.getUnitDao().queryBuilder()
                                         .where( UnitDao.Properties.Name
                                                         .eq( mUnitCtrl.getText().toString() ) ).list();

            Substance substance;
            Unit unit;
            float amount;

            if ( substances.size() == 0 || units.size() == 0 )
            {
                //todo: add new substance (with question)
                if ( substances.size() == 0 )
                {
                    Toast.makeText( getActivity(), "Select substance", Toast.LENGTH_SHORT ).show();
                }
                else
                {
                    Toast.makeText( getActivity(), "Select unit", Toast.LENGTH_SHORT ).show();
                }
            }
            else
            {
                substance = substances.get( 0 );
                unit = units.get( 0 );

                amount = Float.parseFloat( mAmountCtrl.getText().toString() );

                mListener.onAddIngredient( substance, unit, amount );

                mDaoHelper.db_dump();

                dismiss();
            }
        }
    }
}