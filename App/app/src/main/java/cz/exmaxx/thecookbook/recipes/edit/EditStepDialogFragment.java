package cz.exmaxx.thecookbook.recipes.edit;

import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import cz.exmaxx.thecookbook.R;
import cz.exmaxx.thecookbook.db.Step;

public class EditStepDialogFragment extends DialogFragment
{
    private OnActionListener mListener;
    private Step             mStep;
    private boolean          mIsCreationMode;
    private View             mRootView;

    public static EditStepDialogFragment newInstance( OnActionListener listener ) {
        EditStepDialogFragment fragment = new EditStepDialogFragment();
        fragment.mListener = listener;

        return fragment;
    }

    public static EditStepDialogFragment newInstance( OnActionListener listener, Step step ) {
        EditStepDialogFragment fragment = newInstance( listener );

        fragment.mStep = step;

        return fragment;
    }

    public EditStepDialogFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView( LayoutInflater inflater, ViewGroup container,
                              Bundle savedInstanceState ) {
        detemineMode();

        mRootView = inflater.inflate( R.layout.dlg_add_step, container, false );

        initControls();

        return mRootView;
    }

    private void initControls() {
        getDialog().setTitle( isCreationMode() ? "Create Step" : "Change Step" );

        initButton( R.id.btn_add );

        if ( !isCreationMode() ) {
            EditText edtDescription = (EditText) mRootView.findViewById( R.id.edt_description );
            edtDescription.setText( mStep.getDescription() );

            EditText edtTitle = (EditText) mRootView.findViewById( R.id.edt_title );
            edtTitle.setText( mStep.getTitle() );
        } else {

        }
    }

    private void detemineMode() {
        if ( mStep == null )
            mIsCreationMode = true;
        else
            mIsCreationMode = false;
    }

    public void initButton( int id ) {
        Button btn;
        btn = ((Button) mRootView.findViewById( id ));

        switch ( id ) {
            case R.id.btn_add:
                if ( !isCreationMode() )
                    btn.setText( R.string.change );

                btn.setOnClickListener( new View.OnClickListener()
                {
                    @Override
                    public void onClick( View v ) {
                        EditText edtDescription = ((EditText) mRootView
                                .findViewById( R.id.edt_description ));
                        EditText edtTitle = ((EditText) mRootView
                                .findViewById( R.id.edt_title ));

                        if ( isCreationMode() )
                            mStep = new Step();

                        mStep.setTitle( edtTitle.getText().toString() );
                        mStep.setDescription( edtDescription.getText().toString() );

                        mListener.onBtnAddClickInDialog( mStep );
                        dismiss();
                        /*EditStepDialogFragment.this.dismiss();*/ // wow! this really suprised me :D
                    }
                } );
                break;

            default:
                break;
        }
    }

    public boolean isCreationMode() {
        return mIsCreationMode;
    }

    public interface OnActionListener
    {
        public void onBtnAddClickInDialog( Step step );
    }

}