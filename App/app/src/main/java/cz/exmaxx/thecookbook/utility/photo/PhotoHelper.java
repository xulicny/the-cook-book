package cz.exmaxx.thecookbook.utility.photo;

import android.graphics.Bitmap;
import android.media.ExifInterface;
import android.os.Environment;
import android.widget.ImageView;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by maxx on 14.11.2014.
 */
public class PhotoHelper
{

    public static File createImageFile() throws IOException {
        String timeStamp = new SimpleDateFormat( "yyyyMMdd_HHmmss" ).format( new Date() );
        String imageFileName = "cz.exmaxx.cookbook_" + timeStamp;

        File storageDir = Environment.getExternalStoragePublicDirectory( Environment.DIRECTORY_PICTURES );

        return File.createTempFile( imageFileName, ".jpg", storageDir );
    }

    public static void placePhotoInImageView( ImageView imageView, Photo photo ) {
        if ( imageView == null || photo == null )
            return;

        int height = imageView.getMeasuredHeight();
        int width = imageView.getMeasuredWidth();


        int orientation = photo.getOrientation();

        int degreesToRotate = 0;

        switch ( orientation ) {
            case ExifInterface.ORIENTATION_ROTATE_90:
                degreesToRotate = 90;
                break;
            case ExifInterface.ORIENTATION_ROTATE_180:
                degreesToRotate = 180;
                break;
            case ExifInterface.ORIENTATION_ROTATE_270:
                degreesToRotate = 270;
                break;
            default:
        }


        Bitmap bitmap = null;

        if ( degreesToRotate != 0 )
            bitmap = photo.getBitmap( height, width, degreesToRotate );
        else
            bitmap = photo.getBitmap( height, width );

        imageView.setImageBitmap( bitmap );
    }


}

