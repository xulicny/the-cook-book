package cz.exmaxx.thecookbook.recipes.edit;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.SystemClock;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.Chronometer;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Date;

import cz.exmaxx.thecookbook.R;
import cz.exmaxx.thecookbook.arrayAdapters.StepsDetailArrayAdapter;
import cz.exmaxx.thecookbook.db.DaoHelper;
import cz.exmaxx.thecookbook.db.Ingredient;
import cz.exmaxx.thecookbook.db.Recipe;
import cz.exmaxx.thecookbook.db.Step;
import cz.exmaxx.thecookbook.db.Substance;
import cz.exmaxx.thecookbook.db.Unit;
import cz.exmaxx.thecookbook.recipes.detail.IngredientsArrayAdapter;
import cz.exmaxx.thecookbook.utility.ChangeTimeDialogFragment;
import cz.exmaxx.thecookbook.utility.StopWatch;
import cz.exmaxx.thecookbook.utility.Utility;
import cz.exmaxx.thecookbook.utility.photo.Camera;
import cz.exmaxx.thecookbook.utility.photo.Photo;
import cz.exmaxx.thecookbook.utility.photo.PhotoHelper;


public class RecipeEditFragment extends Fragment implements EditStepDialogFragment.OnActionListener, AddIngredientFragment.OnActionListener
{
    //    public static final String ACTION               = "action";
    public static final String RECIPE_ID            = "RECIPE_ID";
    public static final int    ACTION_CREATE_RECIPE = 1;
    public static final int    ACTION_EDIT_RECIPE   = 2;

    private int       mAction;
    private DaoHelper mDaoHelper;
    private Recipe    mRecipe;
    private View      mRootView;
    private EditText  mTitleEdit;
    private StopWatch mStopWatch;
    private Long      mRecipeId;
    private ListView  mListViewSteps;
    private ListView  mListViewIngredients;
    private Camera    mCamera;

    public static RecipeEditFragment newInstance( Long recipeId ) {
        Bundle args = new Bundle( 1 );
        args.putLong( RECIPE_ID, recipeId );

        RecipeEditFragment fragment = new RecipeEditFragment();
        fragment.setArguments( args );

        return fragment;
    }

    public RecipeEditFragment() {
        // Required empty public constructor
    }

    protected boolean isAction( int actionType ) {return mAction == actionType;}

    private void initActionType() {
        if ( mRecipeId == 0 )
            mAction = ACTION_CREATE_RECIPE;
        else
            mAction = ACTION_EDIT_RECIPE;
    }

    public void hideButton( int id ) {
        Button btn = (Button) mRootView.findViewById( id );

        btn.setVisibility( Button.GONE );
    }

    private void initChronometer() {
        if ( isAction( ACTION_CREATE_RECIPE ) )
            initChronometer( 0 );
        else {
            Step lastStep = mRecipe.getLastStep();

            if ( lastStep != null )
                initChronometer( lastStep.getDurationOfPrevStepsPlusOwn() );
            else
                initChronometer( 0 );
        }
    }

    protected void initTitleEdit() {
        mTitleEdit = ((EditText) mRootView.findViewById( R.id.edt_title ));

        if ( mAction == ACTION_EDIT_RECIPE )
            mTitleEdit.setText( mRecipe.getTitle() );

        mTitleEdit.addTextChangedListener( new TextWatcher()
        {
            @Override
            public void beforeTextChanged( CharSequence s, int start, int count, int after ) {

            }

            @Override
            public void onTextChanged( CharSequence s, int start, int before, int count ) {

            }

            @Override
            public void afterTextChanged( Editable s ) {
                mRecipe.setTitle( s.toString() );

                saveRecipe();
            }
        } );
    }

    protected void saveRecipe() {
        mRecipe.setLastEditDate( new Date() );
        mDaoHelper.getRecipeDao().update( mRecipe );
        mDaoHelper.closeDb();
    }

    protected void initChronometer( long elapsedTime ) {
        Chronometer chronometer = (Chronometer) mRootView.findViewById( R.id.tv_remaining_time );

        mStopWatch = new StopWatch( chronometer );

        if ( isAction( ACTION_CREATE_RECIPE ) )
            mStopWatch.setTime( 0 );
        else {
            mStopWatch.setTime( (int) elapsedTime );
            mStopWatch.redraw();
        }

        mStopWatch.setOnClickListener( new OnStopwatchListener() );

    }

    @Override
    public void onBtnAddClickInDialog( Step step ) {
        mRecipe.refresh();

        if ( step.getId() == null ) //creation mode
        {
            // set duration
            int pastDuration = 0;
            Step lastStep = mRecipe.getLastStep();

            if ( lastStep != null )
                pastDuration = lastStep.getDurationOfPrevStepsPlusOwn();

            int chronoDuration = mStopWatch.getTime().intValue();
            int stepDuration = chronoDuration - pastDuration;

            step.setDuration( stepDuration );

            // set
            step.setRecipeID( mRecipe.getId() );

            // add step
            mRecipe.refresh();

            mDaoHelper.getStepDao().insert( step );
            mRecipe.getStepList().add( step );  //or reloadObject the whole DB to update

        } else {  //change mode
            step.update();
        }

        // trace
        mDaoHelper.db_dump();
        Log.d( "DB INSERT", "New Step, ID: " + step.getId() + "(Recipe ID: " + mRecipe + ")" );

        // close DB
        mDaoHelper.closeDb();

        // refresh StepsListView
        refreshStepsListAdapter();
    }

    @Override
    public void onAddIngredient( Substance substance, Unit unit, float amount ) {
        Ingredient ingredient = new Ingredient();

        ingredient.setSubstance( substance );
        ingredient.setUnit( unit );
        ingredient.setAmount( amount );
        ingredient.setRecipeID( mRecipe.getId() );

        mDaoHelper.getIngredientDao().insert( ingredient );

        mRecipe.refresh(); // otherwise no DB was opened
        mRecipe.getIngredientList().add( ingredient );  //or reloadObject the whole DB to update

        mDaoHelper.closeDb();

        // refresh IngredientsList
        refreshIngredientsListAdapter();
    }

    protected void createRecipe() {
        mRecipe = new Recipe();
        mRecipe.setTitle( "" );
        mRecipe.setCreationDate( new Date() );
        mRecipe.setLastEditDate( new Date() );

        mDaoHelper.getRecipeDao().insert( mRecipe );
        mDaoHelper.closeDb();

        Log.d( "DB INSERT", "New Recipe, ID: " + mRecipe.getId() );

        mDaoHelper.db_dump();
    }

    protected void refreshStepsListAdapter() {
        mRecipe.resetStepList();
        ArrayList<Step> steps = (ArrayList<Step>) mRecipe.getStepList();
        mDaoHelper.closeDb();

        mListViewSteps
                .setAdapter( new StepsDetailArrayAdapter( getActivity(), steps, R.layout.list_item_step_detail ) );
    }

    protected void refreshIngredientsListAdapter() {
        mRecipe.refresh();
        ArrayList<Ingredient> ingredients = (ArrayList<Ingredient>) mRecipe.getIngredientList();
        mDaoHelper.closeDb();

        mListViewIngredients
                .setAdapter( new IngredientsArrayAdapter( getActivity(), R.layout.list_item_ingredient, ingredients ) );
    }

    protected void initButton( int id ) {
        Button btn;
        btn = ((Button) mRootView.findViewById( id ));
        btn.setOnClickListener( new View.OnClickListener()
        {
            @Override
            public void onClick( View v ) {

                switch ( v.getId() ) {
                    case R.id.btn_start_pause:
                        onBtnPauseClick( (Button) v );
                        break;

                    case R.id.btn_ingredient:
                        onBtnIngredientClick();
                        break;


                    case R.id.btn_step:
                        onBtnStepClick();
                        break;

                    case R.id.btn_delete:
                        onBtnDeleteClick();
                        break;

                    default:
                }
            }

        } );
    }

    protected void onBtnDeleteClick() {
        AlertDialog.Builder builder = new AlertDialog.Builder( getActivity() );

        builder.setTitle( "Confirm deletion" )
               .setMessage( "Really delete the recipe?" )
               .setPositiveButton( android.R.string.ok, new DialogInterface.OnClickListener()
               {
                   @Override
                   public void onClick( DialogInterface dialog, int which ) {
                       mDaoHelper.getRecipeDao().delete( mRecipe );
                       mDaoHelper.closeDb();

                       getActivity().finish();

                       Utility.showToast( "Recipe deleted.", getActivity() );
                   }
               } )
               .setNegativeButton( android.R.string.cancel, new DialogInterface.OnClickListener()
               {
                   @Override
                   public void onClick( DialogInterface dialog, int which ) {
                       // ntd
                   }
               } );

        builder.create().show();
    }

    protected void onBtnIngredientClick() {
        AddIngredientFragment dlg = AddIngredientFragment.newInstance( this );
        dlg.show( getFragmentManager(), "AddIngredient" );
        dlg.setStyle( DialogFragment.STYLE_NO_TITLE, android.R.style.Theme_Holo_Light );
    }

    protected void onBtnStepClick() {
        EditStepDialogFragment dlg = EditStepDialogFragment.newInstance( this );
        dlg.show( getFragmentManager(), "Add Step" );
    }

    protected void onBtnPauseClick( Button btn ) {
        if ( btn.getText().equals( getString( R.string.pause ) ) ) {
            // Pause clicked
            mStopWatch.pause();

            btn.setText( R.string.start );
        } else {
            // Start clicked
            mStopWatch.start();

            btn.setText( R.string.pause );
        }
    }

    private void displayKeyboard() {
        // Show keyboard - krutej HACK!
        (new Handler()).postDelayed( new Runnable()
        {

            public void run() {
                mTitleEdit.dispatchTouchEvent( MotionEvent.obtain( SystemClock.uptimeMillis(),
                                                                   SystemClock.uptimeMillis(),
                                                                   MotionEvent.ACTION_DOWN,
                                                                   0,
                                                                   0,
                                                                   0 ) );
                mTitleEdit.dispatchTouchEvent( MotionEvent.obtain( SystemClock.uptimeMillis(),
                                                                   SystemClock.uptimeMillis(),
                                                                   MotionEvent.ACTION_UP,
                                                                   0,
                                                                   0,
                                                                   0 ) );
            }
        }, 200 );
    }

    public void onBackPressed() {
        if ( mStopWatch.isRunning() ) {
            AlertDialog.Builder builder = new AlertDialog.Builder( getActivity() );
            builder.setTitle( "Stopwatch running" )
                   .setMessage( "Really quit?" )
                   .setPositiveButton( android.R.string.ok, new DialogInterface.OnClickListener()
                   {
                       @Override
                       public void onClick( DialogInterface dialog, int which ) {
                           getActivity().finish();
                       }
                   } );
            builder.show();

        } else if ( mRecipe.getTitle().isEmpty() ) {
            String newTitle = mRecipe.generateTitle();
            mRecipe.setCreationDate( new Date() );
            mRecipe.update();
            mDaoHelper.closeDb();

            Utility.showToast( "New title generated. To exit tap BACK again.", Toast.LENGTH_LONG, getActivity() );

            displayKeyboard();
            mTitleEdit.setText( newTitle );
            mTitleEdit.requestFocus();
            mTitleEdit.selectAll();

        } else {
            getActivity().finish();

            Utility.showToast( "Recipe saved.", getActivity() );
        }
    }

    @Override
    public void onActivityResult( int requestCode, int resultCode, Intent data ) {
        if ( resultCode == Activity.RESULT_OK ) {
            switch ( requestCode ) {
                case Camera.REQUEST_IMAGE_CAPTURE:

                    Photo photo = mCamera.getPhoto();

                    mRecipe.setThumbnailPath( photo.getFilePath() );
                    mRecipe.update();
                    mDaoHelper.closeDb();

                    photo.addToGallery( getActivity() );

                    updateRecipeThumbnail( false );

                    break;
            }
        }
    }

    @Override
    public View onCreateView( LayoutInflater inflater, ViewGroup container,
                              Bundle savedInstanceState ) {
        mDaoHelper = new DaoHelper( getActivity() );
        mRecipeId = getArguments().getLong( RECIPE_ID );

        initActionType();

        if ( isAction( ACTION_CREATE_RECIPE ) )
            createRecipe();
        else
            mRecipe = mDaoHelper.getRecipeDao().load( mRecipeId );

        mRootView = inflater.inflate( R.layout.fragment_recipe_edit, container, false );

        initChronometer();
        initButton( R.id.btn_step );
        initTitleEdit();

        initButton( R.id.btn_start_pause );
        initButton( R.id.btn_ingredient );
        initButton( R.id.btn_picture );
        initButton( R.id.btn_delete );

        initThumbnailView();

        initListViewSteps();
        initListViewIngredients();

        return mRootView;
    }

    @Override
    public void onCreateContextMenu( ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo ) {
        if ( v.getId() == R.id.lv_steps ) {
            AdapterView.AdapterContextMenuInfo info = ((AdapterView.AdapterContextMenuInfo) menuInfo);

            menu.setHeaderTitle( "Step " + (info.position + 1) + " selected..." );
            getActivity().getMenuInflater().inflate( R.menu.list_item_step, menu );
        }
    }

    @Override
    public boolean onContextItemSelected( MenuItem item ) {
        AdapterView.AdapterContextMenuInfo info = ((AdapterView.AdapterContextMenuInfo) item.getMenuInfo());
        final Step step = (Step) mListViewSteps.getAdapter().getItem( info.position );

        switch ( item.getItemId() ) {
            case R.id.delete:
                AlertDialog.Builder builder = new AlertDialog.Builder( getActivity() );

                builder.setMessage( "Really delete " + "Step " + (info.position + 1) + "?" )
                       .setPositiveButton( android.R.string.ok, new DialogInterface.OnClickListener()
                       {
                           @Override
                           public void onClick( DialogInterface dialog, int which ) {
                               step.delete();
                               mDaoHelper.closeDb();

                               refreshStepsListAdapter();
                           }
                       } )
                       .setNegativeButton( android.R.string.cancel, new DialogInterface.OnClickListener()
                       {
                           @Override
                           public void onClick( DialogInterface dialog, int which ) {
                               //ntd
                           }
                       } );

                builder.show();
                break;

            case R.id.edit:
                EditStepDialogFragment dlg = EditStepDialogFragment.newInstance( this, step );
                dlg.show( getFragmentManager(), "Change Step" );
                break;

            default:
        }


        return true;
    }

    private void initListViewIngredients() {
        mListViewIngredients = (ListView) mRootView.findViewById( R.id.lv_ingredients );
    }

    private void initListViewSteps() {
        mListViewSteps = (ListView) mRootView.findViewById( R.id.lv_steps );

        registerForContextMenu( mListViewSteps );

        refreshStepsListAdapter();
    }

    private void initThumbnailView() {
        ImageView ivThumbnail = ((ImageView) mRootView.findViewById( R.id.iv_thumbnail ));

        ivThumbnail.setOnClickListener( new View.OnClickListener()
        {
            @Override
            public void onClick( View v ) {
                mCamera = new Camera( RecipeEditFragment.this );
                mCamera.startCameraActivity();
            }
        } );

        updateRecipeThumbnail( true );
    }

    private void updateRecipeThumbnail( boolean calledOnInitialization ) {
        if ( calledOnInitialization ) {
            final ViewTreeObserver vto = mRootView.getViewTreeObserver();

            vto.addOnGlobalLayoutListener( new ViewTreeObserver.OnGlobalLayoutListener()
            {
                @Override
                public void onGlobalLayout() {
                    placePhotoInRecipeThumbnail();

                    ViewTreeObserver vto_inner = mRootView.getViewTreeObserver();
                    vto_inner.removeGlobalOnLayoutListener( this );
                }
            } );
        } else {
            placePhotoInRecipeThumbnail();
        }
    }

    private void placePhotoInRecipeThumbnail() {
        if ( mRecipe.getThumbnailPath() == null || mRecipe.getThumbnailPath().isEmpty() )
            return;

        ImageView ivThumbnail = (ImageView) mRootView.findViewById( R.id.iv_thumbnail );
        Photo photo = Photo.createFromFile( mRecipe.getThumbnailPath() );

        PhotoHelper.placePhotoInImageView( ivThumbnail, photo );
    }

    private class OnStopwatchListener implements View.OnClickListener, ChangeTimeDialogFragment.OnActionListener
    {
        @Override
        public void OnTimeChanged( int seconds ) {
            mStopWatch.setTime( seconds );
        }

        @Override
        public void onClick( View v ) {
            ChangeTimeDialogFragment dialog = new ChangeTimeDialogFragment();

            dialog.setOnActionListener( this );
            dialog.setTime( mStopWatch.getTime().intValue() );
            dialog.show( getActivity().getSupportFragmentManager(),
                         "change_time_dialog" );
        }
    }
}

