package cz.exmaxx.thecookbook.recipes.detail;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

import cz.exmaxx.thecookbook.R;
import cz.exmaxx.thecookbook.arrayAdapters.StepsDetailArrayAdapter;
import cz.exmaxx.thecookbook.db.DaoHelper;
import cz.exmaxx.thecookbook.db.Ingredient;
import cz.exmaxx.thecookbook.db.Recipe;
import cz.exmaxx.thecookbook.db.Step;
import cz.exmaxx.thecookbook.recipes.cooking.CookingActivity;
import cz.exmaxx.thecookbook.recipes.edit.RecipeEditFragment;

public class RecipeDetailFragment extends Fragment
{
    public static final String RECIPE_ID = "RECIPE_ID";

    protected Recipe    mRecipe;
    protected DaoHelper mDaoHelper;
    protected View      mRootView;

    public static RecipeDetailFragment newInstance( Long recipeId ) {
        RecipeDetailFragment fragment = new RecipeDetailFragment();

        Bundle args = new Bundle( 1 );
        args.putLong( RECIPE_ID, recipeId );

        fragment.setArguments( args );

        return fragment;
    }

    public RecipeDetailFragment() {
    }

    @Override
    public View onCreateView( LayoutInflater inflater,
                              ViewGroup container,
                              Bundle savedInstanceState ) {
        Bundle args = getArguments();
        long recipeId = args.getLong( RECIPE_ID );

        mDaoHelper = DaoHelper.create( getActivity() );
        mRecipe = mDaoHelper.getRecipeDao().load( recipeId );
        mDaoHelper.closeDb();

        mRootView = inflater.inflate( R.layout.fragment_recipe_detail, container, false );

        initTitle();

        initIngredientsList();
        initStepsList();

        initListener( R.id.btn_delete );
        initListener( R.id.btn_edit );
        initListener( R.id.btn_cook );

        return mRootView;
    }

    public void onClickEdit() {
        RecipeEditFragment newFragment = RecipeEditFragment.newInstance( mRecipe.getId() );

        getActivity().getSupportFragmentManager().beginTransaction()
                     .replace( R.id.container, newFragment )
                     .addToBackStack( null )
                     .commit();
    }

    public void onClickDelete() {

//      Just to remember possibilities to remove fragment:
//        getActivity().getSupportFragmentManager().popBackStack();
//        getActivity().getSupportFragmentManager().beginTransaction().remove( this ).commit();

        AlertDialog.Builder builder = new AlertDialog.Builder( getActivity() );

        builder.setMessage( "Really delete the recipe?" )
               .setPositiveButton( android.R.string.ok, new DialogInterface.OnClickListener()
               {
                   @Override
                   public void onClick( DialogInterface dialog, int which ) {
                       mDaoHelper.getRecipeDao().delete( mRecipe );
                       mDaoHelper.closeDb();

                       getActivity().finish();

                       Toast.makeText( getActivity(), "Recipe deleted.", Toast.LENGTH_SHORT ).show();
                   }
               } )
               .setNegativeButton( android.R.string.cancel, new DialogInterface.OnClickListener()
               {
                   @Override
                   public void onClick( DialogInterface dialog, int which ) {
                       // ntd
                   }
               } );

        builder.create().show();
    }


    private void onClickCook() {
        Intent intent = new Intent( getActivity(), CookingActivity.class );

        intent.putExtra( CookingActivity.RECIPE_ID, mRecipe.getId() );

        startActivity( intent );
    }

    protected void initListener( final int resource ) {
        Button btn = (Button) mRootView.findViewById( resource );

        btn.setOnClickListener( new View.OnClickListener()
        {
            @Override
            public void onClick( View v ) {
                switch ( resource ) {
                    case R.id.btn_delete:
                        onClickDelete();
                        break;

                    case R.id.btn_edit:
                        onClickEdit();
                        break;

                    case R.id.btn_cook:
                        onClickCook();
                        break;

                    default:
                }

            }
        } );
    }

    protected void initTitle() {
        TextView tvTitle = (TextView) mRootView.findViewById( R.id.tv_title );
        tvTitle.setText( mRecipe.getTitle() );
    }

    protected void initStepsList() {
//        mRecipe = mDaoHelper.reloadObject( mRecipe );
        mRecipe.refresh();
        mRecipe.resetStepList();
        ArrayList<Step> steps = (ArrayList<Step>) mRecipe.getStepList();
        mDaoHelper.closeDb();

        ListView listView = (ListView) mRootView.findViewById( R.id.lv_steps );

        listView.setAdapter( new StepsDetailArrayAdapter( getActivity(),
                                                          steps, R.layout.list_item_step_detail
        ) );
    }

    protected void initIngredientsList() {
        // get data
        mRecipe.refresh();
        ArrayList<Ingredient> ingredients = (ArrayList<Ingredient>) mRecipe.getIngredientList();
        mDaoHelper.closeDb();

        // initialize list view
        ListView listView = (ListView) mRootView.findViewById( R.id.lv_ingredients );
        listView.setAdapter( new IngredientsArrayAdapter( getActivity(),
                                                          R.layout.list_item_ingredient,
                                                          ingredients ) );
    }

}
