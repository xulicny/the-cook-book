package cz.exmaxx.thecookbook;

import android.database.sqlite.SQLiteDatabase;

import java.util.Map;

import de.greenrobot.dao.AbstractDao;
import de.greenrobot.dao.AbstractDaoSession;
import de.greenrobot.dao.identityscope.IdentityScopeType;
import de.greenrobot.dao.internal.DaoConfig;

import cz.exmaxx.thecookbook.Cookbook;

import cz.exmaxx.thecookbook.CookbookDao;

// THIS CODE IS GENERATED BY greenDAO, DO NOT EDIT.

/**
 * {@inheritDoc}
 * 
 * @see de.greenrobot.dao.AbstractDaoSession
 */
public class DaoSession extends AbstractDaoSession {

    private final DaoConfig cookbookDaoConfig;

    private final CookbookDao cookbookDao;

    public DaoSession(SQLiteDatabase db, IdentityScopeType type, Map<Class<? extends AbstractDao<?, ?>>, DaoConfig>
            daoConfigMap) {
        super(db);

        cookbookDaoConfig = daoConfigMap.get(CookbookDao.class).clone();
        cookbookDaoConfig.initIdentityScope(type);

        cookbookDao = new CookbookDao(cookbookDaoConfig, this);

        registerDao(Cookbook.class, cookbookDao);
    }
    
    public void clear() {
        cookbookDaoConfig.getIdentityScope().clear();
    }

    public CookbookDao getCookbookDao() {
        return cookbookDao;
    }

}
