package cz.exmaxx.thecookbook.recipes.detail;

import android.app.Activity;
import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.ArrayList;

import cz.exmaxx.thecookbook.R;
import cz.exmaxx.thecookbook.db.DaoHelper;
import cz.exmaxx.thecookbook.db.Ingredient;

/**
 * Created by maxx on 27.8.2014.
 */
public class IngredientsArrayAdapter extends ArrayAdapter<Ingredient>
{

    private Context               mContext;
    private ArrayList<Ingredient> mIngredients;
    private int                   mViewResource;
    /*private RecipeDetailFragment  mParent;*/

    /**
     * @param context      Context.
     * @param viewResource Resource <b>to which</b> is the data mapped.
     * @param array        Array <b>from which</b> is the data mapped.
     */
    public IngredientsArrayAdapter( Context context, int viewResource, ArrayList<Ingredient> array /*, RecipeDetailFragment parent*/ ) {
        super( context, viewResource, array );

        mContext = context;
        mIngredients = array;
        mViewResource = viewResource;
        /*mParent = parent;*/
    }

    @Override
    public View getView( int position, View convertView, final ViewGroup parent ) {
        Ingredient ingredient = mIngredients.get( position );

        RelativeLayout newView;

        if ( convertView != null ) {
            newView = (RelativeLayout) convertView;
        } else {
            newView = ((RelativeLayout) ((Activity) mContext).getLayoutInflater()
                                                             .inflate( mViewResource, null ));
        }

        TextView tvAmount = (TextView) newView.findViewById( R.id.tv_amount );
        TextView tvSubstance = (TextView) newView.findViewById( R.id.tv_substance );

        DaoHelper daoHelper = DaoHelper.create( getContext() );
        ingredient.refresh();

        tvAmount.setText( ingredient.getAmount() + " " + ingredient.getUnit().getName() );
        tvSubstance.setText( ingredient.getSubstance().getName() );

        daoHelper.closeDb();

        return newView;
    }
}
