package cz.exmaxx.thecookbook.listManagement;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.List;

import cz.exmaxx.thecookbook.R;
import cz.exmaxx.thecookbook.arrayAdapters.UnitArrayAdapter;
import cz.exmaxx.thecookbook.db.Unit;

/**
 * Created by maxx on 26.8.2014.
 */
public class UnitListManagementFragment extends ListManagementFragment implements UnitArrayAdapter.OnBtnActionListener
{
    public UnitListManagementFragment()
    {
    }

    //TODO: blbe navrzeno - viz comment nize
    @Override
    void createList() //better solution available? by this signature I do not decalre what should be changed.
    // If I delete mList init I do not know what to do here.
    // however if I would return value, then the "notify list logic" would not work (it checks original object on change)
    // maybe do some more research.......
    {
        mList = mDaoHelper.getUnitDao().loadAll();
        mDaoHelper.closeDb();
    }

    //TODO: blbe navrzeno - viz comment vyse
    @Override
    void populateList()
    {
        if ( mList == null )
        {
            createList();
        }
        else
        {
            // I only want to change items of the list (without creating new list object) so that I can call Adapter->notifyDataSetChanged()
            List<Unit> units = mDaoHelper.getUnitDao().loadAll();
            mDaoHelper.closeDb();

            mList.clear();

            for ( Unit unit : units )
            {
                mList.add( unit );
            }
        }
    }

    @Override
    void onInsertClicked()
    {
        UnitChangeDialogFragment dlg = UnitChangeDialogFragment.newInstance( this );
        dlg.show( getFragmentManager(), "Add Unit Dialog" );
    }

    @Override
    void objectAdd( Object o )
    {
        try
        {
            Unit unit = (Unit) o;

            mDaoHelper.getUnitDao().insert( unit );
            mDaoHelper.closeDb();

            refreshList();
        }
        catch ( ClassCastException e )
        {
            logCastError( o, e );
        }
    }

    @Override
    void objectChange( Object o )
    {
        try
        {
            Unit unit = (Unit) o;

            mDaoHelper.getUnitDao().update( unit );
            mDaoHelper.closeDb();

            refreshList();
        }
        catch ( ClassCastException e )
        {
            logCastError( o, e );
        }
    }

    @Override
    void objectRemove( Object o )
    {
        try
        {
            Unit unit = (Unit) o;

            mDaoHelper.getUnitDao().delete( unit );
            mDaoHelper.closeDb();

            refreshList();
        }
        catch ( ClassCastException e )
        {
            logCastError( o, e );
        }
    }

    @Override
    public View onCreateView( LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState )
    {
        return super.onCreateView( inflater, container, savedInstanceState );
    }

    @Override
    protected void setAdapter()
    {
        UnitArrayAdapter unitArrayAdapter = new UnitArrayAdapter( getActivity(),
                                                                  mList, R.layout.list_item_unit,
                                                                  this );

        mListView.setAdapter( unitArrayAdapter );
    }


    public void onChangeClicked( Long unitId )
    {
        Unit unit = mDaoHelper.getUnitDao().load( unitId );
        mDaoHelper.closeDb();

        UnitChangeDialogFragment dlg = UnitChangeDialogFragment.newInstance( this, unit );
        dlg.show( getFragmentManager(), "Change Unit Dialog" );
    }


    @Override
    public void onRemoveClicked( final Long unitId )
    {
        AlertDialog.Builder builder = new AlertDialog.Builder( getActivity() );

        builder.setMessage( "Really delete " + "unit" + "?" )
               .setPositiveButton( android.R.string.ok, new DialogInterface.OnClickListener()
               {
                   @Override
                   public void onClick( DialogInterface dialog, int which )
                   {
                       Unit unit = mDaoHelper.getUnitDao().load( unitId );
                       mDaoHelper.closeDb();

                       onUnitDelete( unit );
                   }
               } )
               .setNegativeButton( android.R.string.cancel, new DialogInterface.OnClickListener()
               {
                   @Override
                   public void onClick( DialogInterface dialog, int which )
                   {
                       //ntd
                   }
               } );

        builder.show();
    }

    @Override
    public void onResume()
    {
        super.onResume();
    }

    public void onUnitDelete( Unit unit )
    {
        mDaoHelper.getUnitDao().delete( unit );
        mDaoHelper.closeDb();

        refreshList();
    }
}
