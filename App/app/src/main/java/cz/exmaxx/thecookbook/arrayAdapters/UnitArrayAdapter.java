package cz.exmaxx.thecookbook.arrayAdapters;

import android.app.Activity;
import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.List;

import cz.exmaxx.thecookbook.R;
import cz.exmaxx.thecookbook.db.Unit;
import cz.exmaxx.thecookbook.listManagement.UnitListManagementFragment;

/**
 * Created by maxx on 27.8.2014.
 */
public class UnitArrayAdapter extends ArrayAdapter<Unit>
{

    public interface OnBtnActionListener
    {
        void onChangeClicked( Long unitId );

        void onRemoveClicked( Long unitId );
    }

    private Context                    mContext;
    private List<Unit>                 mUnits;
    private int                        mViewResource;
    private UnitListManagementFragment mParent;

    public UnitArrayAdapter( Context context, List<Unit> units, int connectToViewResource, UnitListManagementFragment parent )
    {
        super( context, connectToViewResource, units );

        mContext = context;
        mUnits = units;
        mViewResource = connectToViewResource;
        mParent = parent;
    }

    @Override
    public View getView( int position, View convertView, final ViewGroup parent )
    {
        final Unit unit = mUnits.get( position );

        RelativeLayout newView;

        if ( convertView != null )
        {
            newView = (RelativeLayout) convertView;
        }
        else
        {
            newView = ((RelativeLayout) ((Activity) mContext).getLayoutInflater()
                                                             .inflate( mViewResource, null ));
        }

        TextView tv1 = (TextView) newView.findViewById( android.R.id.text1 );
        TextView tv2 = (TextView) newView.findViewById( android.R.id.text2 );

        tv1.setText( unit.getName() );
        tv2.setText( "(ID: " + unit.getId() + ")" );

        Button btnChange = (Button) newView.findViewById( R.id.btn_change );
        Button btnRemove = (Button) newView.findViewById( R.id.btn_remove );

        btnChange.setOnClickListener( new View.OnClickListener()
        {
            @Override
            public void onClick( View v )
            {
                mParent.onChangeClicked( unit.getId() );
            }
        } );

        btnRemove.setOnClickListener( new View.OnClickListener()
        {
            @Override
            public void onClick( View v )
            {
                mParent.onRemoveClicked( unit.getId() );
            }
        } );

        return newView;
    }
}
