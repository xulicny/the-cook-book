package cz.exmaxx.thecookbook.listManagement;

import android.app.Activity;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import cz.exmaxx.thecookbook.R;
import cz.exmaxx.thecookbook.db.Unit;

public class UnitChangeDialogFragment extends DialogFragment
{

    Unit                   mUnit;
    ListManagementFragment mManager;

    public static UnitChangeDialogFragment newInstance( ListManagementFragment manager ) {
        UnitChangeDialogFragment newMe = new UnitChangeDialogFragment();
        newMe.mManager = manager;
        newMe.mUnit = new Unit();

        return newMe;
    }

    public static UnitChangeDialogFragment newInstance( ListManagementFragment manager, Unit unit /* to change */ ) {
        UnitChangeDialogFragment newMe = new UnitChangeDialogFragment();
        newMe.mManager = manager;
        newMe.mUnit = unit;

        return newMe;
    }

    public UnitChangeDialogFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView( LayoutInflater inflater, ViewGroup container,
                              Bundle savedInstanceState ) {
        View rootView = inflater.inflate( R.layout.dlg_add_unit, container, false );

        if ( mUnit.getId() != null ) {
            // Get unit name
            EditText unitName = (EditText) rootView.findViewById( R.id.edt_name );

            unitName.setText( mUnit.getName() );

            // Change button title to "Change"
            Button btnAdd = (Button) rootView.findViewById( R.id.btn_add );

            btnAdd.setText( R.string.change );
        }

        getDialog().setTitle( "Unit Description" );

        setBtnListener( rootView, R.id.btn_add );
        setBtnListener( rootView, R.id.btn_cancel );

        return rootView;
    }

    private void setBtnListener( View rootView, int id ) {
        Button btn;
        btn = ((Button) rootView.findViewById( id ));
        btn.setOnClickListener( new View.OnClickListener()
        {
            @Override
            public void onClick( View v ) {
                switch ( v.getId() ) {
                    case R.id.btn_add:

                        EditText editText = ((EditText) getView().findViewById( R.id.edt_name ));
                        String name = editText.getText().toString();

                        if ( name.equals( "" ) ) {
                            Toast.makeText( getActivity(),
                                            "Please fill all information first.",
                                            Toast.LENGTH_SHORT ).show();  //TODO: disable "Add" button
                        } else {
                            mUnit.setName( editText.getText().toString() );

                            if ( mUnit.getId() == null ) {
                                mManager.objectAdd( mUnit );   //in case this doesn't call previous Fragment because it
                                // has been destroyd, it would be necessary to place it to BACKSTACK
                            } else {
                                mManager.objectChange( mUnit );
                            }

                            dismiss();
//                          EditStepDialogFragment.this.dismiss(); // wow! this really suprised me :D
                        }
                        break;

                    case R.id.btn_cancel:

                        dismiss();

                        break;

                    default:
                }

            }
        } );
    }

    @Override
    public void onAttach( Activity activity ) {
        super.onAttach( activity );
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mManager = null;
    }

    @Override
    public void onDismiss( DialogInterface dialog ) {
        super.onDismiss( dialog );

        Log.d( "LIFECYCLE", "UnitAddDialogFragment onDismiss()" );
    }

}
