package cz.exmaxx.thecookbook.listManagement;

import android.app.AlertDialog;
import android.content.DialogInterface;

import java.util.List;

import cz.exmaxx.thecookbook.R;
import cz.exmaxx.thecookbook.arrayAdapters.SubstanceArrayAdapter;
import cz.exmaxx.thecookbook.db.Substance;

/**
 * Created by maxx on 3.9.2014.
 */
public class SubstanceListManagementFragment extends ListManagementFragment implements SubstanceArrayAdapter.OnBtnActionListener
{
    public SubstanceListManagementFragment()
    {
    }

    @Override
    public void createList()
    {
        mList = mDaoHelper.getSubstanceDao().loadAll();
        mDaoHelper.closeDb();
    }

    @Override
    public void populateList()
    {
        if ( mList == null )
        {
            createList();
        }
        else
        {
            // I only want to change items of the list (without creating new list object) so that I can call Adapter->notifyDataSetChanged()
            List<Substance> substances = mDaoHelper.getSubstanceDao().loadAll();
            mDaoHelper.closeDb();

            mList.clear();

            for ( Substance substance : substances )
            {
                mList.add( substance );
            }
        }
    }

    @Override
    void onInsertClicked()
    {
        SubstanceChangeDialogFragment dlg = SubstanceChangeDialogFragment.newInstance( this );
        dlg.show( getFragmentManager(), "Add Substance Dialog" );
    }

    @Override
    void objectAdd( Object o )  //TODO: (refactor)
    {
        try
        {
            Substance substance = (Substance) o;

            mDaoHelper.getSubstanceDao().insert( substance );
            mDaoHelper.closeDb();

            refreshList();
        }
        catch ( ClassCastException e )
        {
            logCastError( o, e );
        }
    }

    @Override
    void objectChange( Object o )   //TODO: (refactor)
    {
        try
        {
            Substance substance = (Substance) o;

            mDaoHelper.getSubstanceDao().update( substance );
            mDaoHelper.closeDb();

            refreshList();
        }
        catch ( ClassCastException e )
        {
            logCastError( o, e );
        }

    }

    @Override
    void objectRemove( Object o )   //TODO: (refactor)
    {
        try
        {
            Substance substance = (Substance) o;

            mDaoHelper.getSubstanceDao().delete( substance );
            mDaoHelper.closeDb();

            refreshList();
        }
        catch ( ClassCastException e )
        {
            logCastError( o, e );
        }

    }

    @Override
    protected void setAdapter()
    {
        SubstanceArrayAdapter substanceArrayAdapter = new SubstanceArrayAdapter( getActivity(),
                                                                                 mList, R.layout.list_item_substance,
                                                                                 this );

        mListView.setAdapter( substanceArrayAdapter );

    }

    public void onChangeClicked( Long substanceId )
    {
        Substance substance = mDaoHelper.getSubstanceDao().load( substanceId );
        mDaoHelper.closeDb();

        SubstanceChangeDialogFragment dlg = SubstanceChangeDialogFragment.newInstance( this, substance );
        dlg.show( getFragmentManager(), "Change Dialog" );

    }

    public void onRemoveClicked( final Long substanceId )
    {
        AlertDialog.Builder builder = new AlertDialog.Builder( getActivity() );

        builder.setMessage( "Really delete " + "substance" + "?" )
               .setPositiveButton( android.R.string.ok, new DialogInterface.OnClickListener()
               {
                   @Override
                   public void onClick( DialogInterface dialog, int which )
                   {
                       Substance substance = mDaoHelper.getSubstanceDao().load( substanceId );
                       mDaoHelper.closeDb();

                       objectRemove( substance );
                   }
               } )
               .setNegativeButton( android.R.string.cancel, new DialogInterface.OnClickListener()
               {
                   @Override
                   public void onClick( DialogInterface dialog, int which )
                   {
                       //ntd
                   }
               } );

        builder.show();
    }
}
